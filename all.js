const gameState = {
    path: [],

    layerBackground: 0,
    layerFootprints: 1,
    layerSprites: 2,
    layerUI: 3,

    start: {x: 0, y: 0},
    end: {x: 0, y: 0},
    locationsData: [],
    checkpointsData: [],
    remainingCheckpoints : [],
    remainingOrderedCheckpoints: [],

    stepCount: 0,
    remainingSteps: 0,

    modalTextContent: "Est-ce que c'est le bon chemin ?",
    score: 0,
    logs: []
    
};

const pictures = [];

let scenario = {};

const scenario01 = {
    verif: {
        debut: {
            1: "9;1"
        },
        fin : {
            1: "1;9"
        },
        segments: {
            1: "7;5/7;5",
            2: "5;7/5;7",
            3: "3;5/3;5",
            4: "5;1/5;1"
        }
    },
    translate : {
        fr: "Comme chaque mercredi après-midi, Simon et Michel se voient pour cuisiner. Aujourd’hui, ils décident de confectionner un gâteau aux pommes chez Simon. La liste d’ingrédients en main, ils doivent définir le chemin le plus rapide pour faire leurs commissions. - Commençons par acheter des pommes au marché ! propose Michel.  - Allons ensuite à la boulangerie pour prendre de la farine et du beurre, ajoute Simon. - Oui et puis dirigeons-nous vers le magasin d’alimentation pour acheter un peu de cannelle ! conclut Michel. Et pour finir, passons chez moi récupérer la plaque à gâteaux. Les courses effectuées, les deux garçons se mettent au travail chez Simon. "
    },
    pictures: {
        1: {
            nomFichier: "house",
            x: "495",
            y: "45",
            nom: {
                0: "Maison de Simon"
            }
        },
        2: {
            nomFichier: "marche",
            x: "395",
            y: "245",
            nom: {
                0: "Marché"
            }
        },
        3: {
            nomFichier: "boulangerie",
            x: "295",
            y: "345",
            nom: {
                0: "Boulangerie"
            }
        },
        4: {
            nomFichier: "magasin",
            x: "195",
            y: "245",
            nom: {
                0: "Magasin d'alimentation"
            }
        },
        5: {
            nomFichier: "home96",
            x: "295",
            y: "45",
            nom: {
                0: "Maison de Michel"
            }
        },
        6: {
            nomFichier: "magasin-bonbons",
            x: "295",
            y: "145",
            nom: {
                0: "Magasin de bonbons"
            }
        },
        7: {
            nomFichier: "poste",
            x: "95",
            y: "145",
            nom: {
                0: "Poste"
            }
        }
    }
};

const GRID_OFFSET = Object.freeze({x: 100, y: 100});

const GAME_HEIGHT = 720;
const GAME_WIDTH = 1280;
const SCALE = GAME_HEIGHT / 720;

const CELL_SIZE = 40;

const REPO_LOAD_URL = 'https://gameacademy.ch/assets/piopl/';

/**
 * @class StepCounter
 * @extends Phaser.GameObjects.Sprite
 * @param {Phaser.Scene} scene - The scene that owns this sprite.
 *  */

class StepCounter extends Phaser.GameObjects.Sprite {
    constructor(scene) {
        super(scene, GAME_WIDTH - (GAME_WIDTH - 12 * CELL_SIZE)/2, 20, 'step-counter');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0);
        this.setDepth(3);
        this._scene = scene;
        this._stepCount = 0;

        this.setAlpha(0); 
        this._stepCounterText = this.scene.add.text(GAME_WIDTH - (GAME_WIDTH - 12 * CELL_SIZE)/2, 100, this._stepCount, { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' });
        this._stepCounterText.setOrigin(0.5, 0);
        this._stepCounterText.setAlpha(0);
        this._minSteps = this.calculateMinSteps();
    }
    get coordX() {
        return this._coordX;
    }
    get coordY() {
        return this._coordY;
    }
    get stepCount() {
        return this._stepCount;
    }

    get minSteps() {
        return this._minSteps;
    }


    /**
     * Increments the step counter
     */
    incrementStepCount() {
        this._stepCount++;
        this._stepCounterText.setText(this._stepCount);
    }

    /**
     * Calculates the minimum number of steps to reach all checkpoints
     */
    calculateMinSteps = () => {
        let minSteps = 0;
        minSteps += Math.abs(gameState.start.x - gameState.checkpointsData[0].x) + Math.abs(gameState.start.y - gameState.checkpointsData[0].y);
        for (let i = 0; i < gameState.checkpointsData.length-1; i++) {
            minSteps += Math.abs(gameState.checkpointsData[i].x - gameState.checkpointsData[i+1].x) + Math.abs(gameState.checkpointsData[i].y - gameState.checkpointsData[i+1].y);
        }
        return minSteps;
    }
}/**
 * @class Cell
 * @extends Phaser.GameObjects.Sprite
 * @description A cell of the grid
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the cell
 * @param {number} coordY - The y coordinate of the cell
 */

class Cell extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {       
        super(scene, 0, 0, 'cell');

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;
        this.scene.add.existing(this);
        this.setOrigin(0, 0);
        this.setDepth(gameState.layerBackground);
        this.disableInteractive();
        this.setAlpha(0);

        this._coordX = coordX;
        this._coordY = coordY;
        this._scene = scene;
        this._isSelected = false;

        this._moveButton;
        this._location; // The location that is on this cell, if there is one

        this.on('pointerdown', this.onPointerDown, this);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get isSelected() {
        return this._isSelected;
    }

    set isSelected(value) {
        this._isSelected = value;
    }

    /**
     * @description Spawns the move button on the cell at the cells coordinates
     */
    spawnButton() {
        this._moveButton = new MoveButton(this._scene, this._coordX, this._coordY);
    }

    /**
     * @description Spawns a location on the cell
     * @param {string} name - The name of the location
     * @param {string} fileName - The name of the file of the location
     */
    spawnLocation(name, fileName) {
        this._location = new Location(this._scene, this._coordX, this._coordY, name, fileName);
    }

    /**
     * @description Spawns the label on the cell
     */
    spawnLabel() {
        const angle = Phaser.Math.Angle.Between(this.x + CELL_SIZE / 2, this.y + CELL_SIZE / 2, CELL_SIZE * 6, CELL_SIZE * 6); // The angle between the cell and the center of the map
        const yOff = Math.sin(angle) * (60 + 90 * Math.abs(Math.cos(angle)));
        const xOff = Math.cos(angle) * (60 + 90 * Math.abs(Math.cos(angle)));
        this._label = this._scene.add.image(this.x + CELL_SIZE / 2 + xOff, this.y + CELL_SIZE / 2 + yOff, 'label');
        this._label.setDepth(gameState.layerUI);
        this._label.setOrigin(0.5, 0.5);
        this._text = this._scene.add.text(this._label.x, this._label.y, this._location.name, { fontSize: '12px', fill: '#000' });
        this._text.setDepth(gameState.layerUI);
        this._text.setOrigin(0.5, 0.5);
        gameState.texts.add(this._text);
    }

    /**
     * @description Destroys the move button of the cell
     */
    destroyOwnButton() {
        if (this._moveButton) this._moveButton.destroy();
    }

    /**
     * @description Destroys the label of the cell
     */
    destroyOwnLabel() {
        if (this._label) this._label.destroy();
        if (this._text) this._text.destroy();
    }

    /**
     * @description When the cell is clicked, the move button is spawned on it
     * @description If there is a location on the cell, the label is spawned
     * @description If the player is moving, nothing happens
     */
    onPointerDown() {
        if(!gameState.player.isMoving)
        {
            this._scene.children.each((child) => {
                if (child instanceof Cell) {
                    child.destroyOwnButton();
                    child.destroyOwnLabel();
                }
            });
            this.spawnButton();
        } else return;
        let action = `Cell at x coord ${this._coordX} and y coord ${this._coordY} is clicked!`;

        if (this._location) {
            action =`Cell at x coord ${this._coordX} and y coord ${this._coordY} is clicked and location label of ${this._location.name} is shown !`;
            this.spawnLabel();
        }
        console.log(action);
        this.logChange(action);
    }

    /**
     * @description Displays the cell
     */
    display() {
        this._scene.add.tween({
            targets: this,
            alpha: 0.5,
            duration: 2000,
            ease: 'Power2',
            onComplete: () => {
                this.setInteractive();
            }
        })
    }

    logChange(action) {
        const now = new Date();
        const currentTime = now.toLocaleString('en-GB', { timeZone: 'Europe/Paris', hour12: false });
        const logEntry = {
            timestamp: currentTime,
            action: action,
            location: this._location ? this._location.name : "",
            coordX: this._coordX,
            coordY: this._coordY
        };
        // Push the log entry into the log array
        gameState.logs.push(logEntry);
    }

}/**
 * @class Checkpoint
 * @extends Phaser.GameObjects.Sprite
 * @description A signifier of a checkpoint in the game
 * @param {Phaser.Scene} scene - The scene that owns this sprite.
 * @param {number} coordX - The x coordinate of the checkpoint
 * @param {number} coordY - The y coordinate of the checkpoint
 * @param {number} number - The number of the checkpoint
 */

class Checkpoint extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, number) {
        super(scene, 0, 0, 'bullet_list');

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;

        this._scene = scene;
        this._coordX = coordX;
        this._coordY = coordY;
        this._x = this.x;
        this._y = this.y;
        this._number = number;  

        this.scene.add.existing(this);
        this.setScale(0);
        this.setOrigin(0.1, 0.1);
        this.setDepth(2);

        this._numberText = this.scene.add.text(this._x + 10, this._y, this._number, { fontFamily: 'Arial', fontSize: 32, color: '#FFFFFF', align: 'center'});
        this._numberText.setOrigin(0, 0);
        this._numberText.setAlpha(0);
        this._numberText.setDepth(3);

        this._scene.add.tween({
            targets: this,
            scaleX: 0.4,
            scaleY: 0.4,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this._numberText.setAlpha(1);
            }
        });
        
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get number() {
        return this._number;
    }

    validateCheckpoint = () => {
        this._scene.add.tween({
            targets: this,
            scaleX: 0,
            scaleY: 0,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this.setTexture('bullet_list_true');
                this.setScale(2);
                this._scene.add.tween({
                    targets: this,
                    scaleX: 0.5,
                    scaleY: 0.5,
                    duration: 500,
                    ease: 'Back.easeOut'
                });
            }
        });
        this._numberText.destroy();
    }
}class Fatigue extends Phaser.GameObjects.Container {

    constructor(scene, x, y, max) {
        super(scene, x, y);
        this.scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;

        this._max = max;
    }
}class Feather extends Phaser.GameObjects.Container {
    constructor(scene, x, y) {
        super(scene, x, y);
        this.scene.add.existing(this);     

        this._scene = scene;

        this.feather = scene.add.image(0, 0, 'feather').setOrigin(0, 1);

        this._x = x;
        this._y = y;

        this.add(this.feather);

        this.sineTween = this._scene.add.tween({
            targets: this.feather,
            x: this.feather.x + 40,
            duration: 100,
            ease: 'Sine.easeInOut',
            yoyo: true,
            repeat: -1
        });
        this.toggleSineTween(false);

    }

    drawLocation(location, delay) {
        const coordX = location.coordX;
        const coordY = location.coordY;
        //Move to the location
        this._scene.add.tween({
            targets: this,
            x: coordX * CELL_SIZE + GRID_OFFSET.x,
            y: coordY * CELL_SIZE + GRID_OFFSET.y,
            duration: 500,
            ease: 'Power2',
            delay: delay,
            //Start sine and move down
            onComplete: () => {
                this.toggleSineTween(true);
                this._scene.add.tween({
                    targets: this.feather,
                    y: this.feather.y + 60,
                    duration: 300,
                    ease: 'Linear',
                    //Move up and stop sine
                    onComplete: () => {
                        this._scene.add.tween({
                            targets: this.feather,
                            y: this.feather.y - 60,
                            duration: 200,
                            ease: 'Linear',
                            onComplete: () => {
                                this.toggleSineTween(false);
                            }
                        });
                    }
                });
            }
        });
    }

    moveBackToOriginalPosition(delay){
        this._scene.add.tween({
            targets: this,
            x: this._x,
            y: this._y,
            duration: 1000,
            ease: 'Power2',
            delay: delay
        });
    }

    toggleSineTween(doMove) {
        if (doMove) {
            this.sineTween.resume();
        } else {
            this.sineTween.pause();
        }
    }
}/**
 * @class Footprint
 * @extends Phaser.GameObjects.Sprite
 * @description Footprint spawned by the player
 * @param {Phaser.Scene} scene - The scene where the footprint is
 * @param {number} coordX - The x coordinate of the footprint
 * @param {number} coordY - The y coordinate of the footprint
 * @param {number} direction - The direction of the footprint
 */

class Footprint extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, direction) {
        super(scene, 0, 0, 'footprints');

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;

        this._coordX = coordX;
        this._coordY = coordY;
        this._direction = direction;

        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setAlpha(0.7);
        this.setScale(0.6);

        switch (direction) {
            case 0:
                this.setOrigin(0, 0);
                this.setAngle(0);
                break;
            case 1:
                this.setOrigin(0, 1);
                this.setAngle(90);
                break;
            case 2:
                this.setOrigin(1, 1);
                this.setAngle(180);
                break;
            case 3:
                this.setOrigin(1, 0);
                this.setAngle(270);
                break;
        }

        gameState.footprints.add(this);

    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }
}
/**
 * @class InstructionPanel
 * @extends Phaser.GameObjects.Container
 * @description Panel that displays the instructions
 * @param {Phaser.Scene} scene - The scene where the panel is
 * @param {number} x - The x coordinate of the panel
 * @param {number} y - The y coordinate of the panel
 * @param {string} text - The text that is displayed on the panel
 */

class InstructionPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y, text, maxSteps) { 

        super(scene, x, y);
        
        this.scene.add.existing(this);
        
        this._scene = scene;
        this._text = text;
        this._x = x;
        this._y = y;
        this._maxSteps = maxSteps;

        this.page = scene.add.image(0, 0, 'book')
        .setOrigin(0, 0)
        .setScale(1.2);
        this.pageText = scene.add.text(590, 90, this._text, {fontFamily: 'Arial', fontSize: 18, color: '#000000', align: 'left', wordWrap: { 
            width: 450, 
            useAdvancedWrap: true }});
        gameState.texts.add(this.pageText);
        
        this.frame = scene.add.image(GRID_OFFSET.x, GRID_OFFSET.y, 'map_frame').setOrigin(0, 0).setAlpha(0.1).setScale(1.05);

        this.bar = scene.add.image(100, 70, 'fatigue').setOrigin(0, 0.5);
        this.inkStrain = scene.add.image(100, 70, 'ink_strain').setOrigin(0.5, 0.5);
        this.stepsText = scene.add.text(100, 70, this._maxSteps, { fontFamily: 'Arial', fontSize: 16, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.tts = new TTSPanel(scene, 610, 40, this._text, 'right');

        this.setPosition(this._x, this._y + GAME_HEIGHT);

        this.add(this.page);
        this.add(this.pageText);
        this.add(this.frame);
        this.add(this.bar);
        this.add(this.inkStrain);
        this.add(this.stepsText);
        this.add(this.tts);
        // this.add(this.buttonClose);

        this._scene.input.on('drag', (pointer, gameObject, dragX, dragY) => {
            gameObject.x = dragX;
            gameObject.y = dragY;
        });
    }

    get destY() {
        return this._y;
    }

    updateSteps(steps) {
        this.stepsText.setText(steps);
        this.bar.setScale(steps/this._maxSteps, 1);
    }
}/**
 * @class Location
 * @extends Phaser.GameObjects.Sprite
 * @description A location on the grid
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the location
 * @param {number} coordY - The y coordinate of the location
 * @param {string} name - The name of the location
 * @param {string} fileName - The name of the file of the location
 */

class Location extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, name, fileName) { 

        super(scene, 0, 0, fileName);

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;

        this._coordX = coordX;
        this._coordY = coordY;  
        this._name = name;
        this._fileName = fileName;

        this.scene.add.existing(this);
        this.setOrigin(0.2, 0.2);
        this.setScale(0.6);
        this.setAlpha(0);

        gameState.locations.add(this);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get name() {
        return this._name;
    }

    getfileName() {
        return this._fileName;
    }

    changeToWhite() {
        this.setTintFill(0xf7f7f7);
    }

    drawMe(delay) {
        this.scene.add.tween({
            targets: this,
            alpha: 1,
            duration: 600,
            ease: 'Cubic.easeIn',
            delay: delay,
        });
    }
}/**
 * @class MenuButton
 * @extends Phaser.GameObjects.Sprite
 * @description Button to display the menu
 * @param {Phaser.Scene} scene - The scene where the button is
 * @param {number} x - The x coordinate of the button
 * @param {number} y - The y coordinate of the button   
 */

class MenuButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'button_menu');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8);   
        
        this._x = x;
        this._y = y;

        this._scene = scene;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        //new InstructionPanel(this._scene, this._x, this._y, gameState.instructions);
        this.destroy();
    }
}/**
 * @class MoveButton
 * @description Button that moves the player to the cell where the button is located
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the button
 * @param {number} coordY - The y coordinate of the button
 */

class MoveButton extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {
        super(scene, 0, 0, 'button_move');
        this.scene.add.existing(this);
        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;
        this.setOrigin(0.1, 0.1);
        this.setScale(0);
        this.setDepth(gameState.layerUI);
        this.setInteractive();

        this._coordX = coordX;
        this._coordY = coordY;
        this._scene = scene;

        this.on('pointerdown', this.onPointerDown, this);
        
        this.scene.tweens.add({
            targets: this,
            scale: 0.6,
            duration: 100,
            ease: 'Back'
        });
    }

    /**
     * When the button is clicked, the player moves to the cell where the button is located
     * Destroy the button and the labels of the cells
     */
    onPointerDown() {
        gameState.player.moveOne(this._coordX, this._coordY);
        let action = `Player moves towards x coord ${this._coordX} and y coord ${this._coordY} !`;
        console.log(action);
        this.logChange(action);
        this.destroy();
        this._scene.children.each((child) => {
            if (child instanceof Cell) {
                child.destroyOwnLabel();
            }
        });
    }

    logChange(action) {
        const now = new Date();
        const currentTime = now.toLocaleString('en-GB', { timeZone: 'Europe/Paris', hour12: false });
        const logEntry = {
            timestamp: currentTime,
            action: action,
            coordX: this._coordX,
            coordY: this._coordY
        };
        // Push the log entry into the log array
        gameState.logs.push(logEntry);
    }

}/**
 * @class PlayButton
 * @extends Phaser.GameObjects.Sprite
 * @description Button to validate the player's path
 * @param {Phaser.Scene} scene - The scene where the button is
 * @param {number} x - The x coordinate of the button
 * @param {number} y - The y coordinate of the button
 * @param {function} callback - The function to call when the button is clicked
 */
class PlayButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, callback) {
        super(scene, x, y, 'button_play');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0);    

        this._scene = scene;

        this._x = x;
        this._y = y;

        this._callback = callback;

        this.on('pointerdown', () => {
            this._callback();
        });

        this.scene.tweens.add({
            targets: this,
            scale: 0.8,
            duration: 500,
            ease: 'Back.easeOut'
        });
    }

    onPointerDown() {
        console.log('PlayButton clicked');
    }
}/**
 * @class Player
 * @extends Phaser.GameObjects.Sprite
 * @description The player controlled by the user
 * @param {Phaser.Scene} _scene - The scene where the player is
 * @param {number} _coordX - The x coordinate of the player
 * @param {number} _coordY - The y coordinate of the player
 */

class Player extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {
        super(scene, 0, 0, 'player');
        this.scene.add.existing(this);
        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;
        this.setOrigin(0.1, 0.1);
        this._coordX = coordX;
        this._coordY = coordY;

        this._destinationX;
        this._destinationY;

        this._moveForward = false; // If true, the player will move on the x axis, if false, on the y axis

        this.setDepth(2);

        this._isMoving = false;
        this._scene = scene;
        this.logPos(); 

        this._scene.anims.create({
            key: 'player',
            frames: this.anims.generateFrameNumbers('player', { start: 0, end: 1 }),
            frameRate: 2,
            repeat: -1
        });
        this.anims.play('player', true);

        this.setScale(0.7);
        this.setAlpha(0);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get isMoving() {
        return this._isMoving;
    }

    /**
     * @returns {Object} - The position of the player
     */
    getPos() {
        return {x: this._coordX, y: this._coordY};
    }

    /**
     * Moves the player up
     */
    moveUp() {
        if (this._coordY > 0) {
            new Footprint(this._scene, this._coordX, this._coordY, 0);
            this._coordY--;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }
    /**
     * Moves the player down
     */
    moveDown() {
        if (this._coordY < 15) {
            new Footprint(this._scene, this._coordX, this._coordY, 2);
            this._coordY++;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }

    /**
     * Moves the player left
     */
    moveLeft() {
        if (this._coordX > 0) {
            new Footprint(this._scene, this._coordX, this._coordY, 3);
            this._coordX--;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }

    /**
     * Moves the player right
     */
    moveRight() {
        if (this._coordX < 15) {
            new Footprint(this._scene, this._coordX, this._coordY, 1);
            this._coordX++;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }
    
    /**
     *  Moves the player to a specific position
     * @param {*} x - The x coordinate
     * @param {*} y - The y coordinate
     */
    tweenPosition(x, y) {
        this._isMoving = true;
        this.tween = this.scene.tweens.add({
            targets: this,
            x: x * CELL_SIZE + GRID_OFFSET.x,
            y: y * CELL_SIZE + GRID_OFFSET.y,
            duration: 200,
            ease: 'Power3',
            onComplete: () => {
                this._isMoving = false;
                this.moveOne(this._destinationX, this._destinationY);
            }
        });
    }

    /**
     * Logs the position of the player and checks if the player has reached a checkpoint
     */
    logPos() {
        gameState.path.push({x: this._coordX, y: this._coordY});

        gameState.remainingCheckpoints.forEach((checkpoint) => {
            if (this._coordX === checkpoint.x && this._coordY === checkpoint.y) {
                let index = gameState.remainingCheckpoints.indexOf(checkpoint);
                gameState.remainingCheckpoints.splice(index, 1);
            }
        });

        if(gameState.remainingOrderedCheckpoints.length > 0) {
            if (this._coordX === gameState.remainingOrderedCheckpoints[0].x && this._coordY === gameState.remainingOrderedCheckpoints[0].y) {
                gameState.remainingOrderedCheckpoints.shift();
            }
        }

        gameState.remainingSteps--;
        gameState.instructionPanel.updateSteps(gameState.remainingSteps);
    }

    /**
     * @returns {boolean} - True if the player is on the border of the map, false otherwise
     */
    isOnBorder() {
        if (this._coordX === 0 || this._coordX === 15 || this._coordY === 0 || this._coordY === 15) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Moves one step towards the destination
     * @param {number} x - The x coordinate of the destination
     * @param {number} y - The y coordinate of the destination
     */
    moveOne(x,y) {

        if(gameState.remainingSteps <= 0) {
            return;
        }

        this._destinationX = x;
        this._destinationY = y;

        if(this._moveForward) {
            if (this._coordX < this._destinationX) {
                this.moveRight();
                } else if (this._coordX > this._destinationX) {
                this.moveLeft();
                } else if (this._coordY < this._destinationY) {
                this.moveDown();
                } else if (this._coordY > this._destinationY) {
                this.moveUp();
                }
        } else {
            if (this._coordY < this._destinationY) {
                this.moveDown();
                } else if (this._coordY > this._destinationY) {
                this.moveUp();
                } else if (this._coordX < this._destinationX) {
                this.moveRight();
                } else if (this._coordX > this._destinationX) {
                this.moveLeft();
                }
        }

        this._moveForward = !this._moveForward;

    }

}   /**
 * @class ResetButton
 * @extends Phaser.GameObjects.Sprite
 * @description Button to reset the exercise
 * @param {Phaser.Scene} scene - The scene where the button is
 * @param {number} x - The x coordinate of the button
 * @param {number} y - The y coordinate of the button
 */
class ResetButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'button_return');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8);    

        this._scene = scene;

        this._x = x;
        this._y = y;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        const manager = this._scene.scene.get('Manager');
        manager.loadExerciseAndStartScene('PlayScene');
    }

}class Stars extends Phaser.GameObjects.Container {
    constructor(scene, x, y, score, direction) {
        super(scene, x, y);
        scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;
        this._score = score;
        this._direction = direction;

        this.star0 = this._scene.add.image(0, 0, 'star_off');
        this.add(this.star0);
        this.star1 = this._scene.add.image(0, 0, 'star_off');
        this.add(this.star1);
        this.star2 = this._scene.add.image(0, 0, 'star_off');
        this.add(this.star2);

        this.starsTable = [this.star0, this.star1, this.star2];

        switch(direction) {
            case 'left':
                this.star0.y = - 150;
                this.star2.y = 150;
                this.starsTable.forEach(star => {
                    star.x = - GAME_WIDTH;
                });
                break;
            case 'right':
                this.star0.y = - 150;
                this.star2.y = 150;
                this.starsTable.forEach(star => {
                    star.x = GAME_WIDTH;
                });
                break;
            case 'top':
                this.star0.x = - 150;
                this.star2.x = 150;
                this.starsTable.forEach(star => {
                    star.y = - GAME_HEIGHT;
                });
                break;
            case 'bottom':
                this.star0.x = - 150;
                this.star2.x = 150;
                this.starsTable.forEach(star => {
                    star.y = GAME_HEIGHT;
                });
                break;
            default:
                break;
        }

        if(score === 2) {
            this.star0.setTexture('star_on');
            this.star1.setTexture('star_on');
            this.star2.setTexture('star_on');
        } else if(score < 2 && score > 1) {
            this.star0.setTexture('star_on');
            this.star1.setTexture('star_on');
            this.star2.setTexture('star_off');
        } else if(score <= 1 && score > 0) {
            this.star0.setTexture('star_on');
            this.star1.setTexture('star_off');
            this.star2.setTexture('star_off');
        }

        this.intro(direction);

    }

    intro(direction) {

        let delay = 0;
        this.starsTable.forEach(star => {
            let destX = star.x;
            let destY = star.y;

            switch(direction) {
                case 'left':
                    destX = 0;
                    break;
                case 'right':
                    destX = 0;
                    break;
                case 'top':
                    destY = 0;
                    break;
                case 'bottom':
                    destY = 0;
                    break;
                default:
                    break;
            }
            
            this._scene.add.tween({
                targets: star,
                x: destX,
                y: destY,
                duration: 500,
                delay: delay,
                ease: 'Back.easeOut'
            });
            delay += 100;
        });
    }
}
/**
 * @class ToolsPanel
 * @extends Phaser.GameObjects.Container
 * @description A panel that contains tools for the game
 * @param {Phaser.Scene} scene - The scene that owns this container
 * @param {float} x - x position of the container
 * @param {float} y - y position of the container
 */

class ToolsPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y) {
        super(scene, x, y);
        scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;

        this._tools = [];

        this._states = {
            CLOSED: 0,
            OPEN: 1,
            ON: 2
        };

        this._state = this._states.CLOSED;
        this._stateOD = this._states.OPEN;
        this._stateSpacing = this._states.OPEN;

        this._buttonOpendyslexic = scene.add.sprite(0, 0, 'button_od')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setAngle(180);

        this._buttonLetterSpacing = scene.add.sprite(0, 0, 'button_spacing')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setAngle(180);

        this._buttonTools = scene.add.sprite(0, 0, 'button_tools')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setInteractive();
        
        this.add(this._buttonLetterSpacing);
        this.add(this._buttonOpendyslexic);
        this.add(this._buttonTools);

        this._offset = this._buttonTools.width * 0.6;

        this._buttonTools.on('pointerdown', () => {
            this._buttonTools.setTexture('button_tools_pressed');
        });

        // Button Tools on clicked
        this._buttonTools.on('pointerup', () => {
            switch(this._state) {
                case this._states.CLOSED:
                    this._state = this._states.OPEN;
                    this.openPanel();
                    this._buttonTools.setTexture('button_round_close');
                    break;
                case this._states.OPEN:
                    this._state = this._states.CLOSED;
                    this.closePanel();
                    this._buttonTools.setTexture('button_tools');
                    break;
            }
        });


        this._buttonOpendyslexic.on('pointerdown', () => {
            this._buttonOpendyslexic.setTexture('button_od_pressed');
        });

        // Button Open Dyslexic on clicked
        this._buttonOpendyslexic.on('pointerup', () => {
            switch(this._stateOD) {
                case this._states.OPEN:
                    this._stateOD = this._states.ON;
                    this._buttonOpendyslexic.setTexture('button_od_on');
                    break;
                case this._states.ON:
                    this._stateOD = this._states.OPEN;
                    this._buttonOpendyslexic.setTexture('button_od');
                    break;
            }
            this.updateTextStyle(false);
            global.util.openDys();
        });

        this._buttonLetterSpacing.on('pointerdown', () => {
            this._buttonLetterSpacing.setTexture('button_spacing_pressed');
        });

        // Button Letter Spacing on clicked
        this._buttonLetterSpacing.on('pointerup', () => {
            switch(this._stateSpacing) {
                case this._states.OPEN:
                    this._stateSpacing = this._states.ON;
                    this._buttonLetterSpacing.setTexture('button_spacing_on');
                    break;
                case this._states.ON:
                    this._stateSpacing = this._states.OPEN;
                    this._buttonLetterSpacing.setTexture('button_spacing');
                    break;
            }
            this.updateTextStyle(true);
            global.util.spaceLetters();
        });

        this.setDepth(100);
    }

    /**
     * Opens the panel
     */
    openPanel() {
        this.scene.tweens.add({
            targets: this._buttonOpendyslexic,
            x: - this._offset,
            angle: 0,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonOpendyslexic.setInteractive();
            }
        });

        this.scene.tweens.add({
            targets: this._buttonLetterSpacing,
            angle: 0,
            x: - this._offset * 2,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonLetterSpacing.setInteractive();
            }
        });
    }

    /**
     * Closes the panel
     */
    closePanel() {
        this.scene.tweens.add({
            targets: this._buttonOpendyslexic,
            x: 0,
            angle: 180,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonOpendyslexic.disableInteractive();
            }
        });

        this.scene.tweens.add({
            targets: this._buttonLetterSpacing,
            x: 0,
            angle: 180,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this._buttonLetterSpacing.disableInteractive();
            }
        });
    }

    /**
     * Updates the text style
     * @param {boolean} isForcingWidth 
     */
    updateTextStyle = (isForcingWidth) => {

        gameState.texts.getChildren().forEach((text) => {

            let defaultWidth = (isForcingWidth) => {
                if (isForcingWidth) {
                    return text.style.wordWrapWidth * 1.6;
                } else {
                    return text.style.wordWrapWidth
                }
            }

            switch(this._stateOD) {
                case this._states.ON:
                    text.setFontFamily('opendyslexic');
                    break;
                case this._states.OPEN:
                    text.setFontFamily('Arial');
                    break;
            }
            
            if(isForcingWidth) {
                switch(this._stateSpacing) {
                    case this._states.ON:
                        text.setLetterSpacing(4);
                        text.setWordWrapWidth(text.style.wordWrapWidth / 1.6);
                        break;
                    case this._states.OPEN:
                        text.setLetterSpacing(0);
                        text.setWordWrapWidth(defaultWidth(isForcingWidth));
                        break;
                }
            }

            text.update();
        });
    }
}/**
 * @class TTSPanel
 * @extends Phaser.GameObjects.Container
 * @description A panel that contains TTS tools
 * @param {Phaser.Scene} scene - The scene that owns this container
 * @param {float} x - x position of the container
 * @param {float} y - y position of the container
 * @param {string} text - Text to be read by the TTS
 * @param {string} direction - Direction of the pause button
 */

class TTSPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y, text, direction) {
        super(scene, x, y);
        scene.add.existing(this);

        this.states = {
            CLOSED: 0,
            OPEN: 1,
            ON: 2
        };

        this._statePlay = this.states.OPEN;
        this._statePause = this.states.CLOSED;

        this._direction = direction;
        if (direction == 'left' || direction == 'right' || direction == 'up' || direction == 'down') {
            this._direction = direction;
        } else {
            this._direction = 'null'
        }
        
        this._text = text;
        this._scene = scene;
        this._isTTSPlaying = false;

        this._buttonPause = this.scene.add.sprite(0, 0, 'button_pause')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setInteractive();

        this._buttonPause.on('pointerdown', this.onPause, this);

        this._buttonTTS = this.scene.add.sprite(0, 0, 'button_tts')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setInteractive();

        this._buttonTTS.on('pointerdown', this.onPlay, this);

        this.add(this._buttonPause);
        this.add(this._buttonTTS);
        
    }

    get isTTSPlaying() {
        return this._isTTSPlaying;
    }

    changeText(text) {
        this._text = text;
    }

    /**
     * @description Play the TTS
     */
    async TTS() {
        try {
            this._isTTSPlaying = true;
            console.log('Playing TTS... ' + this._text + '...');
            const speaking = await global.util.TTSPlay(this._text);
            this._isTTSPlaying = false;
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * @description On play button clicked
     * Switch between play and stop
     */
    onPlay() {
        switch(this._statePlay) {
            case this.states.CLOSED:
                this._statePlay = this.states.OPEN;
                break;
            case this.states.OPEN:
                this._statePlay = this.states.ON;
                this.deployPause(this._direction);
                break;
            case this.states.ON:
                this._statePlay = this.states.OPEN;
                this._isTTSPlaying = false;
                this.closePause();
                break;
        }
        this.updatePlay();
        this.updatePause();
    }

    updatePlay() {
        switch(this._statePlay) {
            case this.states.CLOSED:
                this._buttonTTS.setTexture('button_tts');
                break;
            case this.states.OPEN:
                this._buttonTTS.setTexture('button_tts');
                break;
            case this.states.ON:
                this._buttonTTS.setTexture('button_tts_on');
                if (!this._isTTSPlaying) {
                    this.TTS();
                }
                break;
        }
    }

    onPause() {
        switch(this._statePause) {
            case this.states.CLOSED:
                break;
            case this.states.OPEN:
                this._statePause = this.states.ON;
                try {
                    global.util.TTSPause();
                } catch (error) {
                    console.log('Pausing TTS failed.');
                }
                break;
            case this.states.ON:
                this._statePause = this.states.OPEN;
                try {
                    global.util.TTSResume();
                } catch (error) {
                    console.log('Resuming TTS failed.');
                }
                break;
        }
        this.updatePause();
    }

    updatePause() {
        switch(this._statePause) {
            case this.states.CLOSED:
                this._buttonPause.setTexture('button_pause');
                break;
            case this.states.OPEN:
                this._buttonPause.setTexture('button_pause');
                break;
            case this.states.ON:
                this._buttonPause.setTexture('button_pause_on');
                break;
        }
    }

    deployPause(direction) {
        let x_direction = 0;
        let y_direction = 0;
        let offset = this._buttonPause.width * 0.6;
        this._statePause = this.states.OPEN;

        switch(direction) {
            case 'left':
                x_direction = -offset;
                break;
            case 'right':
                x_direction = offset;
                break;
            case 'up':
                y_direction = -offset;
                break;
            case 'down':
                y_direction = offset;
                break;
        }

        this.scene.add.tween({
            targets: this._buttonPause,
            x: x_direction,
            y: y_direction,
            duration: 300,
            ease: 'Power2'
        });
    }

    closePause() {
        this._statePause = this.states.CLOSED;
        this.scene.add.tween({
            targets: this._buttonPause,
            x: 0,
            y: 0,
            duration: 300,
            ease: 'Power2'
        });
    }

}class Validate extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, modalText, validateCallback) {
        super(scene, x, y, 'button_validate');
        scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;
        this._modalText = modalText;
        this._validateCallback = validateCallback;

        this.setInteractive();
        this.on('pointerdown', function () {
            this.openModal();
        });

        this.setScale(0.8);
    }

    openModal() {
        this.blackRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000, 0.7)
        .setOrigin(0, 0)
        .setInteractive()
        .setDepth(101);
        
        this.modal = this._scene.add.image(GAME_WIDTH / 2, GAME_HEIGHT - 100, 'modal')
        .setOrigin(0.5, 0.5)
        .setDepth(102);
        this._scene.add.tween({
            targets: this.modal,
            y: GAME_HEIGHT / 2,
            duration: 500,
            ease: 'Back.easeOut'
        });

        this.modalText = this._scene.add.text(GAME_WIDTH / 2, GAME_HEIGHT - 100 - 10, this._modalText, { 
            fontFamily: 'Roboto', fontSize: 24, color: '#000000', align: 'center', wordWrap: { width: 400, useAdvancedWrap: true } 
        })
        .setOrigin(0.5, 0.5)
        .setDepth(103);
        this._scene.add.tween({
            targets: this.modalText,
            y: GAME_HEIGHT / 2 - 10,
            duration: 500,
            ease: 'Back.easeOut'
        });

        this.buttonValidate = this._scene.add.sprite(GAME_WIDTH + 200, 420, 'button_validate')
        .setOrigin(0.5, 0.5)
        .setDepth(103)
        .setScale(0.8);
        this.buttonValidate.on('pointerdown', () => {
            this._validateCallback();
        });
        this._scene.add.tween({
            targets: this.buttonValidate,
            x: GAME_WIDTH / 2 + 220,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this.buttonValidate.setInteractive();
            }
        });

        this.buttonReturn = this._scene.add.sprite(-200, 420, 'button_return')
        .setOrigin(0.5, 0.5)
        .setDepth(103)
        .setScale(0.8);
        this.buttonReturn.on('pointerdown', () => {
            this.blackRectangle.destroy();
            this.modal.destroy();
            this.modalText.destroy();
            this.buttonValidate.destroy();
            this.buttonReturn.destroy();
        });
        this._scene.add.tween({
            targets: this.buttonReturn,
            x: GAME_WIDTH / 2 - 220,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this.buttonReturn.setInteractive();
            }
        });

    }

    disable() {
        this.disableInteractive();
        this.setAlpha(0);
    }

    enable() {
        this.setInteractive();
        this.setAlpha(1);
    }

    destroyAll() {
        this.blackRectangle.destroy();
            this.modal.destroy();
            this.modalText.destroy();
            this.buttonValidate.destroy();
            this.buttonReturn.destroy();
            this.destroy();
    }

}

class ValidateButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, validateCallback) {
        super(scene, x, y, 'button_validate');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8);    

        this._scene = scene;
        this._validateCallback = validateCallback;

        this._x = x;
        this._y = y;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        this._modal = new ValidatePanel(this._scene, this._validateCallback);
        this.destroy();
    }

}class ValidatePanel extends Phaser.GameObjects.Container {
    constructor(scene, validateCallback) {
        super(scene, GAME_WIDTH / 2, GAME_HEIGHT / 2);
        scene.add.existing(this);

        this._scene = scene;
        this._validateCallback = validateCallback;
        this.setDepth(10);

        this.blackRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000, 0.7).setOrigin(0.5, 0.5);
        this.blackRectangle.setInteractive();

        this.modal = scene.add.image(0, 0, 'modal').setOrigin(0.5, 0.5);
        this.modalText = scene.add.text(0, - 10 , gameState.modalTextContent, { fontFamily: 'Roboto', fontSize: 24, color: '#000000', align: 'center', wordWrap: { width: 400, useAdvancedWrap: true } }).setOrigin(0.5, 0.5);
        
        this.buttonValidate = scene.add.sprite(this.modal.x + 100, this.modal.y + this.modal.height / 2, 'button_validate').setOrigin(0.5, 0.5).setDepth(gameState.layerModal).setScale(0.8);
        this.buttonValidate.on('pointerdown', () => {
            this._validateCallback();
            this.closePanel(false);
        });

        this.buttonReturn = scene.add.sprite(this.modal.x - 100, this.modal.y + this.modal.height / 2, 'button_return').setOrigin(0.5, 0.5).setDepth(gameState.layerModal).setScale(0.8);
        this.buttonReturn.on('pointerdown', () => {
            this.closePanel(true);
        });

        this.add(this.blackRectangle);
        this.add(this.modal);
        this.add(this.modalText);
        this.add(this.buttonValidate);
        this.add(this.buttonReturn);
 

        this.setScale(0);
        this.openPanel();    
    }

    /**
     * Open the panel with a scale animation
     */
    openPanel() {
        this._scene.tweens.add({
            targets: this,
            scale: 1,
            duration: 300,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this.buttonValidate.setInteractive();
                this.buttonReturn.setInteractive();
            }
        });
    }

    /**
     * Close the panel with a scale animation and create a validate button if needed
     * @param {boolean} mustCreateValidateButton 
     */
    closePanel(mustCreateValidateButton){
        this._scene.tweens.add({
            targets: this,
            scale: 0,
            duration: 300,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this.destroy();
                if(mustCreateValidateButton){
                    new ValidateButton(this._scene, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.5);
                }
                
            }
        });
    }

    validateAnswer() {
        this.buttonValidate.disableInteractive();
        this.buttonReturn.disableInteractive();
        this.faddingRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000).setOrigin(0, 0).setDepth(10).setAlpha(0);
        this._scene.tweens.add({
            targets: this.faddingRectangle,
            alpha: 1,
            duration: 1000,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this._scene.scene.stop(this._scene.scene.key);
                this._scene.scene.start('EndScene');
            }
        });
    }
}/**
 * @class Manager
 * @extends Phaser.Scene
 * @description Manager scene is the first scene that is loaded when the game starts. It is responsible for loading the game mode and the scenario.
 */

class Manager extends Phaser.Scene {

    constructor() {
        super({key: 'Manager', active: true});

        try {
            global.util.callOnGamemodeChange(this.loadGameMode);
        } catch (error) {
            console.log("No game mode is selected." + error);

        }
      }
    
    preload() {

        try {
            this.loadGameMode();
        } catch (error) {
            console.log("Loading select scene because global is not defined " + error);
            this.loadScenario(scenario01);
            this.scene.start('PlayScene');
        }

    }

    create() {
        console.log("Manager scene created.");
    }

    update() {
        
    }

    /**
     * @returns {Promise} - A promise that resolves when the local exercise is fetched
     */
    async fetchedLocalExercise() {
        return fetch(REPO_LOAD_URL + 'village-exercise.json')
        .then(response => response.json())
        .then(data => {
            scenario = data;
        });
    }

    /**
     * @description Load the exercise and start the scene
     * @param {string} sceneName - The name of the scene to load
     * @param {string} mapName - The name of the map to load
     */
    async loadExerciseAndStartScene(sceneName, mapName) {
        let exercise;

        try {
            if(forced){
                exercise = global.resources.getExerciceById(forced);
                exercise = JSON.parse(exercise.exercice);
            } else {
                exercise = global.resources.getExercice();
                exercise = JSON.parse(exercise.exercice);
            }
        } catch {
            console.log("No exercise exists. Test mode. Fetching default scenario.");
            await this.fetchedLocalExercise();
            exercise = scenario;
            exercise.map = mapName;
        }

        this.loadScenario(exercise);
        this.scene.start(sceneName);
    }

    /**
     * @description Load the scenario
     * @param {object} scenario 
     */
    loadScenario = (scenario) => {

        // load the start position
        gameState.start.x = parseInt(scenario.verif.debut[1].split(";")[0]);
        gameState.start.y = parseInt(scenario.verif.debut[1].split(";")[1]);
        
        // load the end position
        gameState.end.x = parseInt(scenario.verif.fin[1].split(";")[0]);
        gameState.end.y = parseInt(scenario.verif.fin[1].split(";")[1]);
        
        // load the locations
        gameState.locationsData = [];
        for(let i in scenario.pictures){
            let child = scenario.pictures[i];
            gameState.locationsData.push(child);
            pictures.push(child.nomFichier);
            i++;
        }

        // load the checkpoints in three different arrays
        gameState.checkpointsData = [];
        gameState.remainingCheckpoints = [];
        for(let i in scenario.verif.segments){
            let child = scenario.verif.segments[i];
            let coord = child.split("/")[0]; 
            let cX = parseInt(coord.split(";")[0]);
            let cY = parseInt(coord.split(";")[1]);
            child = {x: cX, y: cY};
            gameState.checkpointsData.push(child); 
            gameState.remainingCheckpoints.push(child);
            gameState.remainingOrderedCheckpoints.push(child);
            i++;
        }

        gameState.instructions = scenario.translate.fr;
       
    }

    /**
     * @description Load the game mode
     */
    loadGameMode = () => {
        let gameMode = global.util.getGamemode();
        // Stop all scenes except the manager
        this.scene.manager.scenes.forEach(scene => {
            if(scene.scene.key !== 'Manager')
            {
                scene.scene.stop();
            }
        });

        switch(gameMode) {
            case global.Gamemode.Evaluate:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Train:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Explore:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
                
            case global.Gamemode.Create:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
        }
    }

    showTutorialInput = (flag) => { 
        try {
            if(flag) {
                // global.util.showTutorialInputs('btnUpload', 'upload_file');
            } else {
                // global.util.hideTutorialInputs('btnUpload', 'upload_file');
            }
        } catch (error) {
            console.log("Error in showTutorialInput: " + error);
        }
    }

}class GameScene extends Phaser.Scene {

    constructor(key) {
        super(key);
    }

    preload(){

    }

    create(){

    }

    update(){

    }

    createElements = () => {

        this.add.image(0, 0, 'background_wood').setOrigin(0, 0).setDepth(gameState.layerBackground);
        
        gameState.texts = this.add.group();

        //Create the right panel
        gameState.stepCounter = new StepCounter(this);
        gameState.stepCounter.calculateMinSteps();

        gameState.validate = new Validate(this, 1210, 645, 'Valides-tu ce chemin ?', () => {
            this.validation();
        });

        gameState.resetButton = new ResetButton(this, GAME_WIDTH - 70, GAME_HEIGHT - 200);

        gameState.remainingSteps = Math.round(this.calculateMinSteps() * 1.3);

        gameState.instructionPanel = new InstructionPanel(this, 0, 0, gameState.instructions, gameState.remainingSteps);

        //Create the cells
        gameState.cells = this.add.group();
        for (let i = 0; i < 10; i++) {
            for (let j = 0; j < 10; j++) {
                let cell = new Cell(this, i, j);
                gameState.cells.add(cell);
            }
        }

        //Create the player
        gameState.player = new Player(this, gameState.start.x, gameState.start.y);
        gameState.footprints = this.add.group();

        //Create the locations
        gameState.locations = this.add.group();
        gameState.locationsData.forEach(location => {

            //We dot this operation because of the old data model. 
            let coordX = ((parseInt(location.x) + 5) / 50) - 1;
            let coordY = (parseInt(location.y) + 5) / 50;
            let fileName = location.nomFichier;
            let name = location.nom[0];

            //Iterate throught the cells. If one has the same coordinates as the location, spawn the location on it.           
            gameState.cells.getChildren().forEach(cell => {
                if (cell.coordX == coordX && cell.coordY == coordY) {
                    cell.spawnLocation(name, fileName);
                }
            });
        });

        //Create the checkpoints
        gameState.checkpoints = this.add.group();
        gameState.toolsPanel = new ToolsPanel(this, GAME_WIDTH - 50, 50);

        //Create the fatigue bar
        gameState.fatigue = new Fatigue(this, 50, 50, 100);


        //Create the feather and draw the locations
        gameState.feather = new Feather(this, 1080, 500);
        gameState.feather.setDepth(gameState.layerUI);
        this.intro();

    }

    intro(){

        let drawDelay = 800;

        this.add.tween({
            targets: gameState.instructionPanel,
            y: gameState.instructionPanel.destY,
            duration: 800,
            ease: 'Quad.easeOut',
            repeat: 0,
            onComplete: () => {
                gameState.cells.getChildren().forEach(cell => {
                    cell.display();
                });
                gameState.locations.getChildren().forEach(location => {
                    gameState.feather.drawLocation(location, drawDelay);
                    location.drawMe(drawDelay + 500);
                    drawDelay += 1000;
                });
                gameState.player.setScale(3);
                this.add.tween({
                    targets: gameState.player,
                    alpha: 1,
                    scale: 0.7,
                    duration: 1000,
                    ease: 'Back.easeOut'
                });
                gameState.feather.moveBackToOriginalPosition(drawDelay + 500);
            }
        });
    }

    /**
     * Calculates the minimum number of steps to reach all checkpoints
     */
        calculateMinSteps = () => {
            let minSteps = 0;
            minSteps += Math.abs(gameState.start.x - gameState.checkpointsData[0].x) + Math.abs(gameState.start.y - gameState.checkpointsData[0].y);
            for (let i = 0; i < gameState.checkpointsData.length-1; i++) {
                minSteps += Math.abs(gameState.checkpointsData[i].x - gameState.checkpointsData[i+1].x) + Math.abs(gameState.checkpointsData[i].y - gameState.checkpointsData[i+1].y);
            }
            return minSteps;
        }

    validation() {
        gameState.validate.destroyAll();
        gameState.resetButton.destroy();

        switch(gameState.remainingCheckpoints.length) {
            case 0:
                gameState.score = 2;
                break;
            case 1:
                gameState.score = 1.5;
                break;
            case 2:
                gameState.score = 1;
                break;
            default:
                gameState.score = 0;
                break;
        }

        gameState.cells.getChildren().forEach(cell => {
            cell.disableInteractive();
        });
        let index = 0;
        gameState.checkpoints = this.add.group();
        new Footprint(this, gameState.player.getPos().x,  gameState.player.getPos().y, 2);

        gameState.checkpointsData.forEach(checkpoint => {
            let cp = new Checkpoint(this, checkpoint.x, checkpoint.y, index + 1);
            gameState.checkpoints.add(cp);
            
            index++;
        });

        let delay = 1500;
        gameState.footprints.getChildren().forEach(footprint => {
            delay += 150;
            this.tweens.add({
                targets: footprint,
                alpha: 0,
                duration: 200,
                delay: delay,
                ease: 'Back.easeIn',
                onComplete: () => {
                    gameState.checkpoints.getChildren().forEach(checkpoint => {
                        if(checkpoint.coordX == footprint.coordX && checkpoint.coordY == footprint.coordY) {
                            checkpoint.validateCheckpoint();
                        }
                    });
                }
            });
        });

        this.time.addEvent({
            delay: delay + 500,
            callback: () => {
                gameState.playButton = new PlayButton(this, GAME_WIDTH - 60, GAME_HEIGHT - 60, () => {
                    gameState.playButton.destroy();
                    this.updateStats(gameState.score);
                });
                this.add.tween({
                    targets: gameState.feather,
                    x: 1400,
                    duration: 500,
                    ease: 'Power2',
                    onComplete: () => {
                        gameState.feather.destroy();
                        gameState.stars = new Stars(this, 1200, 350, gameState.score, 'right');
                    }
                });
            }
        });
    }

    /**
     * @description Display a text on the panel
     * @param {string} text 
     */
    displayTextOnPanel = (text) => {
        let textContent = "<p>" + text + "</p>";
        try {
            global.util.setTutorialText(textContent);
        } catch (error) {         
            console.log(error);
        }
    }

    /**
     * @description Add stats to the statistics
     * @param {int} mode Mode of the game (0: Create, 1: Play, 2: Evaluate)
     */
    addStats = (mode) => {
        try {
            global.statistics.addStats(mode);
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * @description Update the statistics
     * @param {int} score Score to update the statistics
     */
    updateStats = (score) => {
        try {
            global.statistics.updateStats(score, this.getLogsAsJSON());
        } catch (error) {
            console.log('Update stats: ' + score);
        }
    }

    getLogsAsJSON() {
        return JSON.stringify(gameState.logs, null, 2);
    }


    /**
     * @description Update the statistics after a certain amount of time
     * @param {int} score Score to update the statistics
     * @param {float} seconds Seconds to wait before updating the statistics
     */
    updateStatsTimer = (score, seconds) => {
        this.time.addEvent({
            time: seconds * 1000,
            callback: () => {
                this.updateStats(score);
                console.log('Stats updated');
            }
        });
    }
}class CreateScene extends GameScene {
    constructor() {
        super({ key: 'CreateScene' });
    }

    preload() {

    }

    create() {
        console.log("Create scene created.");
    }

    update() {

    }
}class EndScene extends GameScene {
    constructor() {
        super({ key: 'EndScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);
        this.load.spritesheet('playerend', 'player_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.image('checkpoint', 'button_star_on.png');
        this.load.image('checkpoint_off', 'button_star_off.png');
        this.load.image('step-counter', 'step-counter.png');
        this.load.image('modal-end', 'modal.png');
    }

    create() {

        const destinationX = 600;
        const offset = 100;
        this.footprints = 0;

        this.player = this.add.sprite(100, 300, 'playerend');

        this.player.anims.create({
            key: 'playerend',
            frames: this.anims.generateFrameNumbers('playerend', { start: 0, end: 1 }),
            frameRate: 2,
            repeat: -1
        });
        this.player.anims.play('playerend', true);

        this.footprintImage = this.add.image(0, 200, 'step-counter');
        this.footprintText = this.add.text(0, 120, '0', { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.add.text(destinationX, 400, gameState.stepCounter.minSteps, { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.add.image(destinationX, 500, 'checkpoint').setScale(0.5);
        this.add.image(destinationX, 580, 'checkpoint').setScale(0.5);
        this.add.image(destinationX, 660, 'checkpoint').setScale(0.5);

        this.add.image(destinationX + offset, 500, 'checkpoint').setScale(0.5);
        this.add.image(destinationX + offset, 580, 'checkpoint').setScale(0.5);
        
        this.add.image(destinationX + 2 * offset, 500, 'checkpoint').setScale(0.5);

        this.startTimer();

        if(gameState.remainingCheckpoints.length === 0) {
            if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps) {
                console.log('You win! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Bravo! Tu as réussi avec le minimum de pas!');
                this.movePlayerUpdate(destinationX, 2);
            } else if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps * 1.3) {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Tu as réussi avec un peu plus de pas!');
                this.movePlayerUpdate(destinationX + offset, 1.5);
            } else if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps * 1.5) {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Tu as réussi avec un peu plus de pas!');
                this.movePlayerUpdate(destinationX + 2 * offset, 1);
            } else {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Zut! Tu as utilisé trop de pas!');
                this.movePlayerUpdate(destinationX + 2 * offset, 0.5);
            }
        } else {
            console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
            this.popModal('Zut! Tu as manqué une étape!');
            this.movePlayerUpdate(destinationX - 2 * offset, 0);
        }

    }

    update() {
        this.footprintImage.x = this.player.x;
        this.footprintText.x = this.player.x;
    }

    movePlayerUpdate(destination, score) {
        this.add.tween({
            targets: this.player,
            x: destination,
            duration: 3000,
            ease: 'Linear',
            onComplete: () => {
                this.player.anims.stop();
                this.updateStatsTimer(score, 3);
            }
        });
    }

    startTimer() {
        this.time.addEvent({
            delay: 3000 / gameState.stepCounter.stepCount,
            callback: () => {
                this.footprints++;
                this.footprintText.setText(this.footprints);
                if(this.footprints < gameState.stepCounter.stepCount) {
                    this.startTimer();
                }
            }
        });
    }

    popModal(text) {
        this.modalContainer = this.add.container(0, GAME_HEIGHT);
        this.modal = this.add.image(20, -50, 'modal-end');
        this.modal.setOrigin(0, 1);

        this.textModal = this.add.text(65, -145, text, { fontFamily: 'Arial', fontSize: 22, color: '#000000', align: 'center', wordWrap: { width: 400 } });
        this.textModal.setOrigin(0, 1);
        
        this.modalContainer.add(this.modal);
        this.modalContainer.add(this.textModal);
        this.modalContainer.setScale(0, 0);

        this.add.tween({
            targets: this.modalContainer,
            scaleX: 1,
            scaleY: 1,
            duration: 500,
            ease: 'Bounce',
            delay: 4000
        });
    }
}class ExploreScene extends GameScene {
    constructor() {
        super({ key: 'ExploreScene' });
    }

    preload() {

    }

    create() {
        console.log("Explore scene created.");
    }

    update() {

    }
}class PlayScene extends GameScene {
    constructor() {
        super({ key: 'PlayScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);

        this.load.image('background_wood', 'background_wood.png');
        this.load.image('bg_plains', 'cosy-12x12.png');
        this.load.image('cell', 'cell40.png');
        this.load.spritesheet('player', 'player_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.spritesheet('player_shadow', 'player_shadow_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.image('footprints', 'footprint.png');
        this.load.image('button_move', 'button_move.png');
        this.load.image('label', 'label.png');
        this.load.image('button_validate', 'button_validate.png');
        this.load.image('button_return', 'button_return.png');
        this.load.image('button_square_off', 'button_square_off.png');    
        this.load.image('modal', 'modal.png');
        this.load.image('page_big', 'page_big.png');
        this.load.image('button_menu', 'button_menu.png');
        this.load.image('button_close', 'button_close.png');
        this.load.image('button_play', 'button_play.png');
        
        this.load.image('bullet_list', 'bullet_list.png');
        this.load.image('bullet_list_false', 'bullet_list_false.png');
        this.load.image('bullet_list_true', 'bullet_list_true.png');

        this.load.image('button_od', 'button_od.png');
        this.load.image('button_od_pressed', 'button_od_pressed.png');
        this.load.image('button_od_on', 'button_od_on.png');
        this.load.image('button_spacing', 'button_spacing.png');
        this.load.image('button_spacing_pressed', 'button_spacing_pressed.png');
        this.load.image('button_spacing_on', 'button_spacing_on.png');
        this.load.image('button_tools', 'button_tools.png');
        this.load.image('button_tools_pressed', 'button_tools_pressed.png');
        this.load.image('button_tools_on', 'button_tools_on.png');
        this.load.image('button_round_close', 'button_round_close.png');

        this.load.image('button_tts', 'button_tts.png');
        this.load.image('button_tts_pressed', 'button_tts_pressed.png');
        this.load.image('button_tts_on', 'button_tts_on.png');
        this.load.image('button_pause', 'button_pause.png');
        this.load.image('button_pause_pressed', 'button_pause_pressed.png');
        this.load.image('button_pause_on', 'button_pause_on.png');

        this.load.image('ink_strain', 'ink_strain.png');
        this.load.image('fatigue', 'fatigue.png');

        this.load.image('book', 'book.png');
        this.load.image('feather', 'feather.png');
        this.load.image('map_frame', 'map_frame.png');

        this.load.image('star_on', 'star_on.png');
        this.load.image('star_off', 'star_off.png');

        for (let i = 0; i < pictures.length; i++) {
            this.load.image(pictures[i], pictures[i]+'.png');
        }
        
        this.load.image('step-counter', 'step-counter.png');
    }

    create() {
        console.log("Play scene created.");
        this.createElements();
        this.addStats(2);
        this.displayTextOnPanel(gameState.instructions);
    }

    update() {
        
    }

    shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

}const config = {
    type: Phaser.AUTO,
    backgroundColor: "000000",
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'phaser-example',
        autoCenter: Phaser.Scale.CENTER_HORIZONTALLY,
        width: GAME_WIDTH,
        height: GAME_HEIGHT,
        min: {
            width: 960,
            height: 540
        },
        max: {
            width: 960,
            height: 540
        }
    },
    scene: [Manager, GameScene, ExploreScene, PlayScene, CreateScene, EndScene]
};

const app = new Phaser.Game(config);
const global = {};
global.canvas = Canvas.getInstance();
global.Log = Log;
global.util = Util.getInstance();
global.resources = Resources;
global.statistics = Statistics;
global.Gamemode = Gamemode;
global.listenerManager = ListenerManager.getInstance();
