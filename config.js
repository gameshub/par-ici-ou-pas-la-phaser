const config = {
    type: Phaser.AUTO,
    backgroundColor: "000000",
    scale: {
        mode: Phaser.Scale.FIT,
        parent: 'phaser-example',
        autoCenter: Phaser.Scale.CENTER_HORIZONTALLY,
        width: GAME_WIDTH,
        height: GAME_HEIGHT,
        min: {
            width: 960,
            height: 540
        },
        max: {
            width: 960,
            height: 540
        }
    },
    scene: [Manager, GameScene, ExploreScene, PlayScene, CreateScene, EndScene]
};

const app = new Phaser.Game(config);