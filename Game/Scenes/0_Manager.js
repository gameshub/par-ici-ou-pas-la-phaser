/**
 * @class Manager
 * @extends Phaser.Scene
 * @description Manager scene is the first scene that is loaded when the game starts. It is responsible for loading the game mode and the scenario.
 */

class Manager extends Phaser.Scene {

    constructor() {
        super({key: 'Manager', active: true});

        try {
            global.util.callOnGamemodeChange(this.loadGameMode);
        } catch (error) {
            console.log("No game mode is selected." + error);

        }
      }
    
    preload() {

        try {
            this.loadGameMode();
        } catch (error) {
            console.log("Loading select scene because global is not defined " + error);
            this.loadScenario(scenario01);
            this.scene.start('PlayScene');
        }

    }

    create() {
        console.log("Manager scene created.");
    }

    update() {
        
    }

    /**
     * @returns {Promise} - A promise that resolves when the local exercise is fetched
     */
    async fetchedLocalExercise() {
        return fetch(REPO_LOAD_URL + 'village-exercise.json')
        .then(response => response.json())
        .then(data => {
            scenario = data;
        });
    }

    /**
     * @description Load the exercise and start the scene
     * @param {string} sceneName - The name of the scene to load
     * @param {string} mapName - The name of the map to load
     */
    async loadExerciseAndStartScene(sceneName, mapName) {
        let exercise;

        try {
            if(forced){
                exercise = global.resources.getExerciceById(forced);
                exercise = JSON.parse(exercise.exercice);
            } else {
                exercise = global.resources.getExercice();
                exercise = JSON.parse(exercise.exercice);
            }
        } catch {
            console.log("No exercise exists. Test mode. Fetching default scenario.");
            await this.fetchedLocalExercise();
            exercise = scenario;
            exercise.map = mapName;
        }

        this.loadScenario(exercise);
        this.scene.start(sceneName);
    }

    /**
     * @description Load the scenario
     * @param {object} scenario 
     */
    loadScenario = (scenario) => {

        // load the start position
        gameState.start.x = parseInt(scenario.verif.debut[1].split(";")[0]);
        gameState.start.y = parseInt(scenario.verif.debut[1].split(";")[1]);
        
        // load the end position
        gameState.end.x = parseInt(scenario.verif.fin[1].split(";")[0]);
        gameState.end.y = parseInt(scenario.verif.fin[1].split(";")[1]);
        
        // load the locations
        gameState.locationsData = [];
        for(let i in scenario.pictures){
            let child = scenario.pictures[i];
            gameState.locationsData.push(child);
            pictures.push(child.nomFichier);
            i++;
        }

        // load the checkpoints in three different arrays
        gameState.checkpointsData = [];
        gameState.remainingCheckpoints = [];
        for(let i in scenario.verif.segments){
            let child = scenario.verif.segments[i];
            let coord = child.split("/")[0]; 
            let cX = parseInt(coord.split(";")[0]);
            let cY = parseInt(coord.split(";")[1]);
            child = {x: cX, y: cY};
            gameState.checkpointsData.push(child); 
            gameState.remainingCheckpoints.push(child);
            gameState.remainingOrderedCheckpoints.push(child);
            i++;
        }

        gameState.instructions = scenario.translate.fr;
       
    }

    /**
     * @description Load the game mode
     */
    loadGameMode = () => {
        let gameMode = global.util.getGamemode();
        // Stop all scenes except the manager
        this.scene.manager.scenes.forEach(scene => {
            if(scene.scene.key !== 'Manager')
            {
                scene.scene.stop();
            }
        });

        switch(gameMode) {
            case global.Gamemode.Evaluate:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Train:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;

            case global.Gamemode.Explore:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
                
            case global.Gamemode.Create:
                this.showTutorialInput(false);
                this.loadExerciseAndStartScene('PlayScene');
                break;
        }
    }

    showTutorialInput = (flag) => { 
        try {
            if(flag) {
                // global.util.showTutorialInputs('btnUpload', 'upload_file');
            } else {
                // global.util.hideTutorialInputs('btnUpload', 'upload_file');
            }
        } catch (error) {
            console.log("Error in showTutorialInput: " + error);
        }
    }

}