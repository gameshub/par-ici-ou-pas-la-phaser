class EndScene extends GameScene {
    constructor() {
        super({ key: 'EndScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);
        this.load.spritesheet('playerend', 'player_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.image('checkpoint', 'button_star_on.png');
        this.load.image('checkpoint_off', 'button_star_off.png');
        this.load.image('step-counter', 'step-counter.png');
        this.load.image('modal-end', 'modal.png');
    }

    create() {

        const destinationX = 600;
        const offset = 100;
        this.footprints = 0;

        this.player = this.add.sprite(100, 300, 'playerend');

        this.player.anims.create({
            key: 'playerend',
            frames: this.anims.generateFrameNumbers('playerend', { start: 0, end: 1 }),
            frameRate: 2,
            repeat: -1
        });
        this.player.anims.play('playerend', true);

        this.footprintImage = this.add.image(0, 200, 'step-counter');
        this.footprintText = this.add.text(0, 120, '0', { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.add.text(destinationX, 400, gameState.stepCounter.minSteps, { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.add.image(destinationX, 500, 'checkpoint').setScale(0.5);
        this.add.image(destinationX, 580, 'checkpoint').setScale(0.5);
        this.add.image(destinationX, 660, 'checkpoint').setScale(0.5);

        this.add.image(destinationX + offset, 500, 'checkpoint').setScale(0.5);
        this.add.image(destinationX + offset, 580, 'checkpoint').setScale(0.5);
        
        this.add.image(destinationX + 2 * offset, 500, 'checkpoint').setScale(0.5);

        this.startTimer();

        if(gameState.remainingCheckpoints.length === 0) {
            if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps) {
                console.log('You win! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Bravo! Tu as réussi avec le minimum de pas!');
                this.movePlayerUpdate(destinationX, 2);
            } else if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps * 1.3) {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Tu as réussi avec un peu plus de pas!');
                this.movePlayerUpdate(destinationX + offset, 1.5);
            } else if(gameState.stepCounter.stepCount <= gameState.stepCounter.minSteps * 1.5) {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Tu as réussi avec un peu plus de pas!');
                this.movePlayerUpdate(destinationX + 2 * offset, 1);
            } else {
                console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
                this.popModal('Zut! Tu as utilisé trop de pas!');
                this.movePlayerUpdate(destinationX + 2 * offset, 0.5);
            }
        } else {
            console.log('You lose! ' + ' Steps: ' + gameState.stepCounter.stepCount + ' Min steps: ' + gameState.stepCounter.minSteps);
            this.popModal('Zut! Tu as manqué une étape!');
            this.movePlayerUpdate(destinationX - 2 * offset, 0);
        }

    }

    update() {
        this.footprintImage.x = this.player.x;
        this.footprintText.x = this.player.x;
    }

    movePlayerUpdate(destination, score) {
        this.add.tween({
            targets: this.player,
            x: destination,
            duration: 3000,
            ease: 'Linear',
            onComplete: () => {
                this.player.anims.stop();
                this.updateStatsTimer(score, 3);
            }
        });
    }

    startTimer() {
        this.time.addEvent({
            delay: 3000 / gameState.stepCounter.stepCount,
            callback: () => {
                this.footprints++;
                this.footprintText.setText(this.footprints);
                if(this.footprints < gameState.stepCounter.stepCount) {
                    this.startTimer();
                }
            }
        });
    }

    popModal(text) {
        this.modalContainer = this.add.container(0, GAME_HEIGHT);
        this.modal = this.add.image(20, -50, 'modal-end');
        this.modal.setOrigin(0, 1);

        this.textModal = this.add.text(65, -145, text, { fontFamily: 'Arial', fontSize: 22, color: '#000000', align: 'center', wordWrap: { width: 400 } });
        this.textModal.setOrigin(0, 1);
        
        this.modalContainer.add(this.modal);
        this.modalContainer.add(this.textModal);
        this.modalContainer.setScale(0, 0);

        this.add.tween({
            targets: this.modalContainer,
            scaleX: 1,
            scaleY: 1,
            duration: 500,
            ease: 'Bounce',
            delay: 4000
        });
    }
}