class GameScene extends Phaser.Scene {

    constructor(key) {
        super(key);
    }

    preload(){

    }

    create(){

    }

    update(){

    }

    createElements = () => {

        this.add.image(0, 0, 'background_wood').setOrigin(0, 0).setDepth(gameState.layerBackground);
        
        gameState.texts = this.add.group();

        //Create the right panel
        gameState.stepCounter = new StepCounter(this);
        gameState.stepCounter.calculateMinSteps();

        gameState.validate = new Validate(this, 1210, 645, 'Valides-tu ce chemin ?', () => {
            this.validation();
        });

        gameState.resetButton = new ResetButton(this, GAME_WIDTH - 70, GAME_HEIGHT - 200);

        gameState.remainingSteps = Math.round(this.calculateMinSteps() * 1.3);

        gameState.instructionPanel = new InstructionPanel(this, 0, 0, gameState.instructions, gameState.remainingSteps);

        //Create the cells
        gameState.cells = this.add.group();
        for (let i = 0; i < 10; i++) {
            for (let j = 0; j < 10; j++) {
                let cell = new Cell(this, i, j);
                gameState.cells.add(cell);
            }
        }

        //Create the player
        gameState.player = new Player(this, gameState.start.x, gameState.start.y);
        gameState.footprints = this.add.group();

        //Create the locations
        gameState.locations = this.add.group();
        gameState.locationsData.forEach(location => {

            //We dot this operation because of the old data model. 
            let coordX = ((parseInt(location.x) + 5) / 50) - 1;
            let coordY = (parseInt(location.y) + 5) / 50;
            let fileName = location.nomFichier;
            let name = location.nom[0];

            //Iterate throught the cells. If one has the same coordinates as the location, spawn the location on it.           
            gameState.cells.getChildren().forEach(cell => {
                if (cell.coordX == coordX && cell.coordY == coordY) {
                    cell.spawnLocation(name, fileName);
                }
            });
        });

        //Create the checkpoints
        gameState.checkpoints = this.add.group();
        gameState.toolsPanel = new ToolsPanel(this, GAME_WIDTH - 50, 50);

        //Create the fatigue bar
        gameState.fatigue = new Fatigue(this, 50, 50, 100);


        //Create the feather and draw the locations
        gameState.feather = new Feather(this, 1080, 500);
        gameState.feather.setDepth(gameState.layerUI);
        this.intro();

    }

    intro(){

        let drawDelay = 800;

        this.add.tween({
            targets: gameState.instructionPanel,
            y: gameState.instructionPanel.destY,
            duration: 800,
            ease: 'Quad.easeOut',
            repeat: 0,
            onComplete: () => {
                gameState.cells.getChildren().forEach(cell => {
                    cell.display();
                });
                gameState.locations.getChildren().forEach(location => {
                    gameState.feather.drawLocation(location, drawDelay);
                    location.drawMe(drawDelay + 500);
                    drawDelay += 1000;
                });
                gameState.player.setScale(3);
                this.add.tween({
                    targets: gameState.player,
                    alpha: 1,
                    scale: 0.7,
                    duration: 1000,
                    ease: 'Back.easeOut'
                });
                gameState.feather.moveBackToOriginalPosition(drawDelay + 500);
            }
        });
    }

    /**
     * Calculates the minimum number of steps to reach all checkpoints
     */
        calculateMinSteps = () => {
            let minSteps = 0;
            minSteps += Math.abs(gameState.start.x - gameState.checkpointsData[0].x) + Math.abs(gameState.start.y - gameState.checkpointsData[0].y);
            for (let i = 0; i < gameState.checkpointsData.length-1; i++) {
                minSteps += Math.abs(gameState.checkpointsData[i].x - gameState.checkpointsData[i+1].x) + Math.abs(gameState.checkpointsData[i].y - gameState.checkpointsData[i+1].y);
            }
            return minSteps;
        }

    validation() {
        gameState.validate.destroyAll();
        gameState.resetButton.destroy();

        switch(gameState.remainingCheckpoints.length) {
            case 0:
                gameState.score = 2;
                break;
            case 1:
                gameState.score = 1.5;
                break;
            case 2:
                gameState.score = 1;
                break;
            default:
                gameState.score = 0;
                break;
        }

        gameState.cells.getChildren().forEach(cell => {
            cell.disableInteractive();
        });
        let index = 0;
        gameState.checkpoints = this.add.group();
        new Footprint(this, gameState.player.getPos().x,  gameState.player.getPos().y, 2);

        gameState.checkpointsData.forEach(checkpoint => {
            let cp = new Checkpoint(this, checkpoint.x, checkpoint.y, index + 1);
            gameState.checkpoints.add(cp);
            
            index++;
        });

        let delay = 1500;
        gameState.footprints.getChildren().forEach(footprint => {
            delay += 150;
            this.tweens.add({
                targets: footprint,
                alpha: 0,
                duration: 200,
                delay: delay,
                ease: 'Back.easeIn',
                onComplete: () => {
                    gameState.checkpoints.getChildren().forEach(checkpoint => {
                        if(checkpoint.coordX == footprint.coordX && checkpoint.coordY == footprint.coordY) {
                            checkpoint.validateCheckpoint();
                        }
                    });
                }
            });
        });

        this.time.addEvent({
            delay: delay + 500,
            callback: () => {
                gameState.playButton = new PlayButton(this, GAME_WIDTH - 60, GAME_HEIGHT - 60, () => {
                    gameState.playButton.destroy();
                    this.updateStats(gameState.score);
                });
                this.add.tween({
                    targets: gameState.feather,
                    x: 1400,
                    duration: 500,
                    ease: 'Power2',
                    onComplete: () => {
                        gameState.feather.destroy();
                        gameState.stars = new Stars(this, 1200, 350, gameState.score, 'right');
                    }
                });
            }
        });
    }

    /**
     * @description Display a text on the panel
     * @param {string} text 
     */
    displayTextOnPanel = (text) => {
        let textContent = "<p>" + text + "</p>";
        try {
            global.util.setTutorialText(textContent);
        } catch (error) {         
            console.log(error);
        }
    }

    /**
     * @description Add stats to the statistics
     * @param {int} mode Mode of the game (0: Create, 1: Play, 2: Evaluate)
     */
    addStats = (mode) => {
        try {
            global.statistics.addStats(mode);
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * @description Update the statistics
     * @param {int} score Score to update the statistics
     */
    updateStats = (score) => {
        try {
            global.statistics.updateStats(score, this.getLogsAsJSON());
        } catch (error) {
            console.log('Update stats: ' + score);
        }
    }

    getLogsAsJSON() {
        return JSON.stringify(gameState.logs, null, 2);
    }


    /**
     * @description Update the statistics after a certain amount of time
     * @param {int} score Score to update the statistics
     * @param {float} seconds Seconds to wait before updating the statistics
     */
    updateStatsTimer = (score, seconds) => {
        this.time.addEvent({
            time: seconds * 1000,
            callback: () => {
                this.updateStats(score);
                console.log('Stats updated');
            }
        });
    }
}