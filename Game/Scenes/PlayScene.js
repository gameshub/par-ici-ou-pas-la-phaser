class PlayScene extends GameScene {
    constructor() {
        super({ key: 'PlayScene' });
    }

    preload() {
        this.load.setBaseURL(REPO_LOAD_URL);

        this.load.image('background_wood', 'background_wood.png');
        this.load.image('bg_plains', 'cosy-12x12.png');
        this.load.image('cell', 'cell40.png');
        this.load.spritesheet('player', 'player_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.spritesheet('player_shadow', 'player_shadow_ss.png', { frameWidth: 80, frameHeight: 80 });
        this.load.image('footprints', 'footprint.png');
        this.load.image('button_move', 'button_move.png');
        this.load.image('label', 'label.png');
        this.load.image('button_validate', 'button_validate.png');
        this.load.image('button_return', 'button_return.png');
        this.load.image('button_square_off', 'button_square_off.png');    
        this.load.image('modal', 'modal.png');
        this.load.image('page_big', 'page_big.png');
        this.load.image('button_menu', 'button_menu.png');
        this.load.image('button_close', 'button_close.png');
        this.load.image('button_play', 'button_play.png');
        
        this.load.image('bullet_list', 'bullet_list.png');
        this.load.image('bullet_list_false', 'bullet_list_false.png');
        this.load.image('bullet_list_true', 'bullet_list_true.png');

        this.load.image('button_od', 'button_od.png');
        this.load.image('button_od_pressed', 'button_od_pressed.png');
        this.load.image('button_od_on', 'button_od_on.png');
        this.load.image('button_spacing', 'button_spacing.png');
        this.load.image('button_spacing_pressed', 'button_spacing_pressed.png');
        this.load.image('button_spacing_on', 'button_spacing_on.png');
        this.load.image('button_tools', 'button_tools.png');
        this.load.image('button_tools_pressed', 'button_tools_pressed.png');
        this.load.image('button_tools_on', 'button_tools_on.png');
        this.load.image('button_round_close', 'button_round_close.png');

        this.load.image('button_tts', 'button_tts.png');
        this.load.image('button_tts_pressed', 'button_tts_pressed.png');
        this.load.image('button_tts_on', 'button_tts_on.png');
        this.load.image('button_pause', 'button_pause.png');
        this.load.image('button_pause_pressed', 'button_pause_pressed.png');
        this.load.image('button_pause_on', 'button_pause_on.png');

        this.load.image('ink_strain', 'ink_strain.png');
        this.load.image('fatigue', 'fatigue.png');

        this.load.image('book', 'book.png');
        this.load.image('feather', 'feather.png');
        this.load.image('map_frame', 'map_frame.png');

        this.load.image('star_on', 'star_on.png');
        this.load.image('star_off', 'star_off.png');

        for (let i = 0; i < pictures.length; i++) {
            this.load.image(pictures[i], pictures[i]+'.png');
        }
        
        this.load.image('step-counter', 'step-counter.png');
    }

    create() {
        console.log("Play scene created.");
        this.createElements();
        this.addStats(2);
        this.displayTextOnPanel(gameState.instructions);
    }

    update() {
        
    }

    shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

}