const gameState = {
    path: [],

    layerBackground: 0,
    layerFootprints: 1,
    layerSprites: 2,
    layerUI: 3,

    start: {x: 0, y: 0},
    end: {x: 0, y: 0},
    locationsData: [],
    checkpointsData: [],
    remainingCheckpoints : [],
    remainingOrderedCheckpoints: [],

    stepCount: 0,
    remainingSteps: 0,

    modalTextContent: "Est-ce que c'est le bon chemin ?",
    score: 0,
    logs: []
    
};

const pictures = [];

let scenario = {};

const scenario01 = {
    verif: {
        debut: {
            1: "9;1"
        },
        fin : {
            1: "1;9"
        },
        segments: {
            1: "7;5/7;5",
            2: "5;7/5;7",
            3: "3;5/3;5",
            4: "5;1/5;1"
        }
    },
    translate : {
        fr: "Comme chaque mercredi après-midi, Simon et Michel se voient pour cuisiner. Aujourd’hui, ils décident de confectionner un gâteau aux pommes chez Simon. La liste d’ingrédients en main, ils doivent définir le chemin le plus rapide pour faire leurs commissions. - Commençons par acheter des pommes au marché ! propose Michel.  - Allons ensuite à la boulangerie pour prendre de la farine et du beurre, ajoute Simon. - Oui et puis dirigeons-nous vers le magasin d’alimentation pour acheter un peu de cannelle ! conclut Michel. Et pour finir, passons chez moi récupérer la plaque à gâteaux. Les courses effectuées, les deux garçons se mettent au travail chez Simon. "
    },
    pictures: {
        1: {
            nomFichier: "house",
            x: "495",
            y: "45",
            nom: {
                0: "Maison de Simon"
            }
        },
        2: {
            nomFichier: "marche",
            x: "395",
            y: "245",
            nom: {
                0: "Marché"
            }
        },
        3: {
            nomFichier: "boulangerie",
            x: "295",
            y: "345",
            nom: {
                0: "Boulangerie"
            }
        },
        4: {
            nomFichier: "magasin",
            x: "195",
            y: "245",
            nom: {
                0: "Magasin d'alimentation"
            }
        },
        5: {
            nomFichier: "home96",
            x: "295",
            y: "45",
            nom: {
                0: "Maison de Michel"
            }
        },
        6: {
            nomFichier: "magasin-bonbons",
            x: "295",
            y: "145",
            nom: {
                0: "Magasin de bonbons"
            }
        },
        7: {
            nomFichier: "poste",
            x: "95",
            y: "145",
            nom: {
                0: "Poste"
            }
        }
    }
};

const GRID_OFFSET = Object.freeze({x: 100, y: 100});

const GAME_HEIGHT = 720;
const GAME_WIDTH = 1280;
const SCALE = GAME_HEIGHT / 720;

const CELL_SIZE = 40;

const REPO_LOAD_URL = 'https://gameacademy.ch/assets/piopl/';

