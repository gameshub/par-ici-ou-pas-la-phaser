class Fatigue extends Phaser.GameObjects.Container {

    constructor(scene, x, y, max) {
        super(scene, x, y);
        this.scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;

        this._max = max;
    }
}