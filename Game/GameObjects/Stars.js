class Stars extends Phaser.GameObjects.Container {
    constructor(scene, x, y, score, direction) {
        super(scene, x, y);
        scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;
        this._score = score;
        this._direction = direction;

        this.star0 = this._scene.add.image(0, 0, 'star_off');
        this.add(this.star0);
        this.star1 = this._scene.add.image(0, 0, 'star_off');
        this.add(this.star1);
        this.star2 = this._scene.add.image(0, 0, 'star_off');
        this.add(this.star2);

        this.starsTable = [this.star0, this.star1, this.star2];

        switch(direction) {
            case 'left':
                this.star0.y = - 150;
                this.star2.y = 150;
                this.starsTable.forEach(star => {
                    star.x = - GAME_WIDTH;
                });
                break;
            case 'right':
                this.star0.y = - 150;
                this.star2.y = 150;
                this.starsTable.forEach(star => {
                    star.x = GAME_WIDTH;
                });
                break;
            case 'top':
                this.star0.x = - 150;
                this.star2.x = 150;
                this.starsTable.forEach(star => {
                    star.y = - GAME_HEIGHT;
                });
                break;
            case 'bottom':
                this.star0.x = - 150;
                this.star2.x = 150;
                this.starsTable.forEach(star => {
                    star.y = GAME_HEIGHT;
                });
                break;
            default:
                break;
        }

        if(score === 2) {
            this.star0.setTexture('star_on');
            this.star1.setTexture('star_on');
            this.star2.setTexture('star_on');
        } else if(score < 2 && score > 1) {
            this.star0.setTexture('star_on');
            this.star1.setTexture('star_on');
            this.star2.setTexture('star_off');
        } else if(score <= 1 && score > 0) {
            this.star0.setTexture('star_on');
            this.star1.setTexture('star_off');
            this.star2.setTexture('star_off');
        }

        this.intro(direction);

    }

    intro(direction) {

        let delay = 0;
        this.starsTable.forEach(star => {
            let destX = star.x;
            let destY = star.y;

            switch(direction) {
                case 'left':
                    destX = 0;
                    break;
                case 'right':
                    destX = 0;
                    break;
                case 'top':
                    destY = 0;
                    break;
                case 'bottom':
                    destY = 0;
                    break;
                default:
                    break;
            }
            
            this._scene.add.tween({
                targets: star,
                x: destX,
                y: destY,
                duration: 500,
                delay: delay,
                ease: 'Back.easeOut'
            });
            delay += 100;
        });
    }
}
