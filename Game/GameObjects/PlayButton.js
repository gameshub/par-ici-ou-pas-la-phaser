/**
 * @class PlayButton
 * @extends Phaser.GameObjects.Sprite
 * @description Button to validate the player's path
 * @param {Phaser.Scene} scene - The scene where the button is
 * @param {number} x - The x coordinate of the button
 * @param {number} y - The y coordinate of the button
 * @param {function} callback - The function to call when the button is clicked
 */
class PlayButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, callback) {
        super(scene, x, y, 'button_play');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0);    

        this._scene = scene;

        this._x = x;
        this._y = y;

        this._callback = callback;

        this.on('pointerdown', () => {
            this._callback();
        });

        this.scene.tweens.add({
            targets: this,
            scale: 0.8,
            duration: 500,
            ease: 'Back.easeOut'
        });
    }

    onPointerDown() {
        console.log('PlayButton clicked');
    }
}