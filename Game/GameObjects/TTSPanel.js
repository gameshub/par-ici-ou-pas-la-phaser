/**
 * @class TTSPanel
 * @extends Phaser.GameObjects.Container
 * @description A panel that contains TTS tools
 * @param {Phaser.Scene} scene - The scene that owns this container
 * @param {float} x - x position of the container
 * @param {float} y - y position of the container
 * @param {string} text - Text to be read by the TTS
 * @param {string} direction - Direction of the pause button
 */

class TTSPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y, text, direction) {
        super(scene, x, y);
        scene.add.existing(this);

        this.states = {
            CLOSED: 0,
            OPEN: 1,
            ON: 2
        };

        this._statePlay = this.states.OPEN;
        this._statePause = this.states.CLOSED;

        this._direction = direction;
        if (direction == 'left' || direction == 'right' || direction == 'up' || direction == 'down') {
            this._direction = direction;
        } else {
            this._direction = 'null'
        }
        
        this._text = text;
        this._scene = scene;
        this._isTTSPlaying = false;

        this._buttonPause = this.scene.add.sprite(0, 0, 'button_pause')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setInteractive();

        this._buttonPause.on('pointerdown', this.onPause, this);

        this._buttonTTS = this.scene.add.sprite(0, 0, 'button_tts')
        .setOrigin(0.5, 0.5)
        .setScale(0.5)
        .setInteractive();

        this._buttonTTS.on('pointerdown', this.onPlay, this);

        this.add(this._buttonPause);
        this.add(this._buttonTTS);
        
    }

    get isTTSPlaying() {
        return this._isTTSPlaying;
    }

    changeText(text) {
        this._text = text;
    }

    /**
     * @description Play the TTS
     */
    async TTS() {
        try {
            this._isTTSPlaying = true;
            console.log('Playing TTS... ' + this._text + '...');
            const speaking = await global.util.TTSPlay(this._text);
            this._isTTSPlaying = false;
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * @description On play button clicked
     * Switch between play and stop
     */
    onPlay() {
        switch(this._statePlay) {
            case this.states.CLOSED:
                this._statePlay = this.states.OPEN;
                break;
            case this.states.OPEN:
                this._statePlay = this.states.ON;
                this.deployPause(this._direction);
                break;
            case this.states.ON:
                this._statePlay = this.states.OPEN;
                this._isTTSPlaying = false;
                this.closePause();
                break;
        }
        this.updatePlay();
        this.updatePause();
    }

    updatePlay() {
        switch(this._statePlay) {
            case this.states.CLOSED:
                this._buttonTTS.setTexture('button_tts');
                break;
            case this.states.OPEN:
                this._buttonTTS.setTexture('button_tts');
                break;
            case this.states.ON:
                this._buttonTTS.setTexture('button_tts_on');
                if (!this._isTTSPlaying) {
                    this.TTS();
                }
                break;
        }
    }

    onPause() {
        switch(this._statePause) {
            case this.states.CLOSED:
                break;
            case this.states.OPEN:
                this._statePause = this.states.ON;
                try {
                    global.util.TTSPause();
                } catch (error) {
                    console.log('Pausing TTS failed.');
                }
                break;
            case this.states.ON:
                this._statePause = this.states.OPEN;
                try {
                    global.util.TTSResume();
                } catch (error) {
                    console.log('Resuming TTS failed.');
                }
                break;
        }
        this.updatePause();
    }

    updatePause() {
        switch(this._statePause) {
            case this.states.CLOSED:
                this._buttonPause.setTexture('button_pause');
                break;
            case this.states.OPEN:
                this._buttonPause.setTexture('button_pause');
                break;
            case this.states.ON:
                this._buttonPause.setTexture('button_pause_on');
                break;
        }
    }

    deployPause(direction) {
        let x_direction = 0;
        let y_direction = 0;
        let offset = this._buttonPause.width * 0.6;
        this._statePause = this.states.OPEN;

        switch(direction) {
            case 'left':
                x_direction = -offset;
                break;
            case 'right':
                x_direction = offset;
                break;
            case 'up':
                y_direction = -offset;
                break;
            case 'down':
                y_direction = offset;
                break;
        }

        this.scene.add.tween({
            targets: this._buttonPause,
            x: x_direction,
            y: y_direction,
            duration: 300,
            ease: 'Power2'
        });
    }

    closePause() {
        this._statePause = this.states.CLOSED;
        this.scene.add.tween({
            targets: this._buttonPause,
            x: 0,
            y: 0,
            duration: 300,
            ease: 'Power2'
        });
    }

}