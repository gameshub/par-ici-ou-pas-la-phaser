

class ValidateButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, validateCallback) {
        super(scene, x, y, 'button_validate');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8);    

        this._scene = scene;
        this._validateCallback = validateCallback;

        this._x = x;
        this._y = y;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        this._modal = new ValidatePanel(this._scene, this._validateCallback);
        this.destroy();
    }

}