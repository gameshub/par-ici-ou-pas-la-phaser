/**
 * @class Checkpoint
 * @extends Phaser.GameObjects.Sprite
 * @description A signifier of a checkpoint in the game
 * @param {Phaser.Scene} scene - The scene that owns this sprite.
 * @param {number} coordX - The x coordinate of the checkpoint
 * @param {number} coordY - The y coordinate of the checkpoint
 * @param {number} number - The number of the checkpoint
 */

class Checkpoint extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, number) {
        super(scene, 0, 0, 'bullet_list');

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;

        this._scene = scene;
        this._coordX = coordX;
        this._coordY = coordY;
        this._x = this.x;
        this._y = this.y;
        this._number = number;  

        this.scene.add.existing(this);
        this.setScale(0);
        this.setOrigin(0.1, 0.1);
        this.setDepth(2);

        this._numberText = this.scene.add.text(this._x + 10, this._y, this._number, { fontFamily: 'Arial', fontSize: 32, color: '#FFFFFF', align: 'center'});
        this._numberText.setOrigin(0, 0);
        this._numberText.setAlpha(0);
        this._numberText.setDepth(3);

        this._scene.add.tween({
            targets: this,
            scaleX: 0.4,
            scaleY: 0.4,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this._numberText.setAlpha(1);
            }
        });
        
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get number() {
        return this._number;
    }

    validateCheckpoint = () => {
        this._scene.add.tween({
            targets: this,
            scaleX: 0,
            scaleY: 0,
            duration: 300,
            ease: 'Power2',
            onComplete: () => {
                this.setTexture('bullet_list_true');
                this.setScale(2);
                this._scene.add.tween({
                    targets: this,
                    scaleX: 0.5,
                    scaleY: 0.5,
                    duration: 500,
                    ease: 'Back.easeOut'
                });
            }
        });
        this._numberText.destroy();
    }
}