/**
 * @class MenuButton
 * @extends Phaser.GameObjects.Sprite
 * @description Button to display the menu
 * @param {Phaser.Scene} scene - The scene where the button is
 * @param {number} x - The x coordinate of the button
 * @param {number} y - The y coordinate of the button   
 */

class MenuButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'button_menu');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8);   
        
        this._x = x;
        this._y = y;

        this._scene = scene;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        //new InstructionPanel(this._scene, this._x, this._y, gameState.instructions);
        this.destroy();
    }
}