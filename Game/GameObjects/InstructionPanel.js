/**
 * @class InstructionPanel
 * @extends Phaser.GameObjects.Container
 * @description Panel that displays the instructions
 * @param {Phaser.Scene} scene - The scene where the panel is
 * @param {number} x - The x coordinate of the panel
 * @param {number} y - The y coordinate of the panel
 * @param {string} text - The text that is displayed on the panel
 */

class InstructionPanel extends Phaser.GameObjects.Container {
    constructor(scene, x, y, text, maxSteps) { 

        super(scene, x, y);
        
        this.scene.add.existing(this);
        
        this._scene = scene;
        this._text = text;
        this._x = x;
        this._y = y;
        this._maxSteps = maxSteps;

        this.page = scene.add.image(0, 0, 'book')
        .setOrigin(0, 0)
        .setScale(1.2);
        this.pageText = scene.add.text(590, 90, this._text, {fontFamily: 'Arial', fontSize: 18, color: '#000000', align: 'left', wordWrap: { 
            width: 450, 
            useAdvancedWrap: true }});
        gameState.texts.add(this.pageText);
        
        this.frame = scene.add.image(GRID_OFFSET.x, GRID_OFFSET.y, 'map_frame').setOrigin(0, 0).setAlpha(0.1).setScale(1.05);

        this.bar = scene.add.image(100, 70, 'fatigue').setOrigin(0, 0.5);
        this.inkStrain = scene.add.image(100, 70, 'ink_strain').setOrigin(0.5, 0.5);
        this.stepsText = scene.add.text(100, 70, this._maxSteps, { fontFamily: 'Arial', fontSize: 16, color: '#ffffff', align: 'center' }).setOrigin(0.5, 0.5);

        this.tts = new TTSPanel(scene, 610, 40, this._text, 'right');

        this.setPosition(this._x, this._y + GAME_HEIGHT);

        this.add(this.page);
        this.add(this.pageText);
        this.add(this.frame);
        this.add(this.bar);
        this.add(this.inkStrain);
        this.add(this.stepsText);
        this.add(this.tts);
        // this.add(this.buttonClose);

        this._scene.input.on('drag', (pointer, gameObject, dragX, dragY) => {
            gameObject.x = dragX;
            gameObject.y = dragY;
        });
    }

    get destY() {
        return this._y;
    }

    updateSteps(steps) {
        this.stepsText.setText(steps);
        this.bar.setScale(steps/this._maxSteps, 1);
    }
}