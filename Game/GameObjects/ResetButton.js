/**
 * @class ResetButton
 * @extends Phaser.GameObjects.Sprite
 * @description Button to reset the exercise
 * @param {Phaser.Scene} scene - The scene where the button is
 * @param {number} x - The x coordinate of the button
 * @param {number} y - The y coordinate of the button
 */
class ResetButton extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'button_return');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setDepth(3);
        this.setInteractive();
        this.setScale(0.8);    

        this._scene = scene;

        this._x = x;
        this._y = y;

        this.on('pointerdown', this.onPointerDown, this);
    }

    onPointerDown() {
        const manager = this._scene.scene.get('Manager');
        manager.loadExerciseAndStartScene('PlayScene');
    }

}