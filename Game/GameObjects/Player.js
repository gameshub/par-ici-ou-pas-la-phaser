/**
 * @class Player
 * @extends Phaser.GameObjects.Sprite
 * @description The player controlled by the user
 * @param {Phaser.Scene} _scene - The scene where the player is
 * @param {number} _coordX - The x coordinate of the player
 * @param {number} _coordY - The y coordinate of the player
 */

class Player extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {
        super(scene, 0, 0, 'player');
        this.scene.add.existing(this);
        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;
        this.setOrigin(0.1, 0.1);
        this._coordX = coordX;
        this._coordY = coordY;

        this._destinationX;
        this._destinationY;

        this._moveForward = false; // If true, the player will move on the x axis, if false, on the y axis

        this.setDepth(2);

        this._isMoving = false;
        this._scene = scene;
        this.logPos(); 

        this._scene.anims.create({
            key: 'player',
            frames: this.anims.generateFrameNumbers('player', { start: 0, end: 1 }),
            frameRate: 2,
            repeat: -1
        });
        this.anims.play('player', true);

        this.setScale(0.7);
        this.setAlpha(0);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get isMoving() {
        return this._isMoving;
    }

    /**
     * @returns {Object} - The position of the player
     */
    getPos() {
        return {x: this._coordX, y: this._coordY};
    }

    /**
     * Moves the player up
     */
    moveUp() {
        if (this._coordY > 0) {
            new Footprint(this._scene, this._coordX, this._coordY, 0);
            this._coordY--;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }
    /**
     * Moves the player down
     */
    moveDown() {
        if (this._coordY < 15) {
            new Footprint(this._scene, this._coordX, this._coordY, 2);
            this._coordY++;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }

    /**
     * Moves the player left
     */
    moveLeft() {
        if (this._coordX > 0) {
            new Footprint(this._scene, this._coordX, this._coordY, 3);
            this._coordX--;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }

    /**
     * Moves the player right
     */
    moveRight() {
        if (this._coordX < 15) {
            new Footprint(this._scene, this._coordX, this._coordY, 1);
            this._coordX++;
            this.tweenPosition(this._coordX, this._coordY);
            gameState.stepCounter.incrementStepCount();
            this.logPos();
        }
    }
    
    /**
     *  Moves the player to a specific position
     * @param {*} x - The x coordinate
     * @param {*} y - The y coordinate
     */
    tweenPosition(x, y) {
        this._isMoving = true;
        this.tween = this.scene.tweens.add({
            targets: this,
            x: x * CELL_SIZE + GRID_OFFSET.x,
            y: y * CELL_SIZE + GRID_OFFSET.y,
            duration: 200,
            ease: 'Power3',
            onComplete: () => {
                this._isMoving = false;
                this.moveOne(this._destinationX, this._destinationY);
            }
        });
    }

    /**
     * Logs the position of the player and checks if the player has reached a checkpoint
     */
    logPos() {
        gameState.path.push({x: this._coordX, y: this._coordY});

        gameState.remainingCheckpoints.forEach((checkpoint) => {
            if (this._coordX === checkpoint.x && this._coordY === checkpoint.y) {
                let index = gameState.remainingCheckpoints.indexOf(checkpoint);
                gameState.remainingCheckpoints.splice(index, 1);
            }
        });

        if(gameState.remainingOrderedCheckpoints.length > 0) {
            if (this._coordX === gameState.remainingOrderedCheckpoints[0].x && this._coordY === gameState.remainingOrderedCheckpoints[0].y) {
                gameState.remainingOrderedCheckpoints.shift();
            }
        }

        gameState.remainingSteps--;
        gameState.instructionPanel.updateSteps(gameState.remainingSteps);
    }

    /**
     * @returns {boolean} - True if the player is on the border of the map, false otherwise
     */
    isOnBorder() {
        if (this._coordX === 0 || this._coordX === 15 || this._coordY === 0 || this._coordY === 15) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Moves one step towards the destination
     * @param {number} x - The x coordinate of the destination
     * @param {number} y - The y coordinate of the destination
     */
    moveOne(x,y) {

        if(gameState.remainingSteps <= 0) {
            return;
        }

        this._destinationX = x;
        this._destinationY = y;

        if(this._moveForward) {
            if (this._coordX < this._destinationX) {
                this.moveRight();
                } else if (this._coordX > this._destinationX) {
                this.moveLeft();
                } else if (this._coordY < this._destinationY) {
                this.moveDown();
                } else if (this._coordY > this._destinationY) {
                this.moveUp();
                }
        } else {
            if (this._coordY < this._destinationY) {
                this.moveDown();
                } else if (this._coordY > this._destinationY) {
                this.moveUp();
                } else if (this._coordX < this._destinationX) {
                this.moveRight();
                } else if (this._coordX > this._destinationX) {
                this.moveLeft();
                }
        }

        this._moveForward = !this._moveForward;

    }

}   