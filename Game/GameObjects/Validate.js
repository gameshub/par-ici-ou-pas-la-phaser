class Validate extends Phaser.GameObjects.Sprite {
    constructor(scene, x, y, modalText, validateCallback) {
        super(scene, x, y, 'button_validate');
        scene.add.existing(this);

        this._scene = scene;
        this._x = x;
        this._y = y;
        this._modalText = modalText;
        this._validateCallback = validateCallback;

        this.setInteractive();
        this.on('pointerdown', function () {
            this.openModal();
        });

        this.setScale(0.8);
    }

    openModal() {
        this.blackRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000, 0.7)
        .setOrigin(0, 0)
        .setInteractive()
        .setDepth(101);
        
        this.modal = this._scene.add.image(GAME_WIDTH / 2, GAME_HEIGHT - 100, 'modal')
        .setOrigin(0.5, 0.5)
        .setDepth(102);
        this._scene.add.tween({
            targets: this.modal,
            y: GAME_HEIGHT / 2,
            duration: 500,
            ease: 'Back.easeOut'
        });

        this.modalText = this._scene.add.text(GAME_WIDTH / 2, GAME_HEIGHT - 100 - 10, this._modalText, { 
            fontFamily: 'Roboto', fontSize: 24, color: '#000000', align: 'center', wordWrap: { width: 400, useAdvancedWrap: true } 
        })
        .setOrigin(0.5, 0.5)
        .setDepth(103);
        this._scene.add.tween({
            targets: this.modalText,
            y: GAME_HEIGHT / 2 - 10,
            duration: 500,
            ease: 'Back.easeOut'
        });

        this.buttonValidate = this._scene.add.sprite(GAME_WIDTH + 200, 420, 'button_validate')
        .setOrigin(0.5, 0.5)
        .setDepth(103)
        .setScale(0.8);
        this.buttonValidate.on('pointerdown', () => {
            this._validateCallback();
        });
        this._scene.add.tween({
            targets: this.buttonValidate,
            x: GAME_WIDTH / 2 + 220,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this.buttonValidate.setInteractive();
            }
        });

        this.buttonReturn = this._scene.add.sprite(-200, 420, 'button_return')
        .setOrigin(0.5, 0.5)
        .setDepth(103)
        .setScale(0.8);
        this.buttonReturn.on('pointerdown', () => {
            this.blackRectangle.destroy();
            this.modal.destroy();
            this.modalText.destroy();
            this.buttonValidate.destroy();
            this.buttonReturn.destroy();
        });
        this._scene.add.tween({
            targets: this.buttonReturn,
            x: GAME_WIDTH / 2 - 220,
            duration: 500,
            ease: 'Back.easeOut',
            onComplete: () => {
                this.buttonReturn.setInteractive();
            }
        });

    }

    disable() {
        this.disableInteractive();
        this.setAlpha(0);
    }

    enable() {
        this.setInteractive();
        this.setAlpha(1);
    }

    destroyAll() {
        this.blackRectangle.destroy();
            this.modal.destroy();
            this.modalText.destroy();
            this.buttonValidate.destroy();
            this.buttonReturn.destroy();
            this.destroy();
    }

}