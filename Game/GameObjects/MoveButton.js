/**
 * @class MoveButton
 * @description Button that moves the player to the cell where the button is located
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the button
 * @param {number} coordY - The y coordinate of the button
 */

class MoveButton extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {
        super(scene, 0, 0, 'button_move');
        this.scene.add.existing(this);
        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;
        this.setOrigin(0.1, 0.1);
        this.setScale(0);
        this.setDepth(gameState.layerUI);
        this.setInteractive();

        this._coordX = coordX;
        this._coordY = coordY;
        this._scene = scene;

        this.on('pointerdown', this.onPointerDown, this);
        
        this.scene.tweens.add({
            targets: this,
            scale: 0.6,
            duration: 100,
            ease: 'Back'
        });
    }

    /**
     * When the button is clicked, the player moves to the cell where the button is located
     * Destroy the button and the labels of the cells
     */
    onPointerDown() {
        gameState.player.moveOne(this._coordX, this._coordY);
        let action = `Player moves towards x coord ${this._coordX} and y coord ${this._coordY} !`;
        console.log(action);
        this.logChange(action);
        this.destroy();
        this._scene.children.each((child) => {
            if (child instanceof Cell) {
                child.destroyOwnLabel();
            }
        });
    }

    logChange(action) {
        const now = new Date();
        const currentTime = now.toLocaleString('en-GB', { timeZone: 'Europe/Paris', hour12: false });
        const logEntry = {
            timestamp: currentTime,
            action: action,
            coordX: this._coordX,
            coordY: this._coordY
        };
        // Push the log entry into the log array
        gameState.logs.push(logEntry);
    }

}