/**
 * @class Cell
 * @extends Phaser.GameObjects.Sprite
 * @description A cell of the grid
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the cell
 * @param {number} coordY - The y coordinate of the cell
 */

class Cell extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY) {       
        super(scene, 0, 0, 'cell');

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;
        this.scene.add.existing(this);
        this.setOrigin(0, 0);
        this.setDepth(gameState.layerBackground);
        this.disableInteractive();
        this.setAlpha(0);

        this._coordX = coordX;
        this._coordY = coordY;
        this._scene = scene;
        this._isSelected = false;

        this._moveButton;
        this._location; // The location that is on this cell, if there is one

        this.on('pointerdown', this.onPointerDown, this);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get isSelected() {
        return this._isSelected;
    }

    set isSelected(value) {
        this._isSelected = value;
    }

    /**
     * @description Spawns the move button on the cell at the cells coordinates
     */
    spawnButton() {
        this._moveButton = new MoveButton(this._scene, this._coordX, this._coordY);
    }

    /**
     * @description Spawns a location on the cell
     * @param {string} name - The name of the location
     * @param {string} fileName - The name of the file of the location
     */
    spawnLocation(name, fileName) {
        this._location = new Location(this._scene, this._coordX, this._coordY, name, fileName);
    }

    /**
     * @description Spawns the label on the cell
     */
    spawnLabel() {
        const angle = Phaser.Math.Angle.Between(this.x + CELL_SIZE / 2, this.y + CELL_SIZE / 2, CELL_SIZE * 6, CELL_SIZE * 6); // The angle between the cell and the center of the map
        const yOff = Math.sin(angle) * (60 + 90 * Math.abs(Math.cos(angle)));
        const xOff = Math.cos(angle) * (60 + 90 * Math.abs(Math.cos(angle)));
        this._label = this._scene.add.image(this.x + CELL_SIZE / 2 + xOff, this.y + CELL_SIZE / 2 + yOff, 'label');
        this._label.setDepth(gameState.layerUI);
        this._label.setOrigin(0.5, 0.5);
        this._text = this._scene.add.text(this._label.x, this._label.y, this._location.name, { fontSize: '12px', fill: '#000' });
        this._text.setDepth(gameState.layerUI);
        this._text.setOrigin(0.5, 0.5);
        gameState.texts.add(this._text);
    }

    /**
     * @description Destroys the move button of the cell
     */
    destroyOwnButton() {
        if (this._moveButton) this._moveButton.destroy();
    }

    /**
     * @description Destroys the label of the cell
     */
    destroyOwnLabel() {
        if (this._label) this._label.destroy();
        if (this._text) this._text.destroy();
    }

    /**
     * @description When the cell is clicked, the move button is spawned on it
     * @description If there is a location on the cell, the label is spawned
     * @description If the player is moving, nothing happens
     */
    onPointerDown() {
        if(!gameState.player.isMoving)
        {
            this._scene.children.each((child) => {
                if (child instanceof Cell) {
                    child.destroyOwnButton();
                    child.destroyOwnLabel();
                }
            });
            this.spawnButton();
        } else return;
        let action = `Cell at x coord ${this._coordX} and y coord ${this._coordY} is clicked!`;

        if (this._location) {
            action =`Cell at x coord ${this._coordX} and y coord ${this._coordY} is clicked and location label of ${this._location.name} is shown !`;
            this.spawnLabel();
        }
        console.log(action);
        this.logChange(action);
    }

    /**
     * @description Displays the cell
     */
    display() {
        this._scene.add.tween({
            targets: this,
            alpha: 0.5,
            duration: 2000,
            ease: 'Power2',
            onComplete: () => {
                this.setInteractive();
            }
        })
    }

    logChange(action) {
        const now = new Date();
        const currentTime = now.toLocaleString('en-GB', { timeZone: 'Europe/Paris', hour12: false });
        const logEntry = {
            timestamp: currentTime,
            action: action,
            location: this._location ? this._location.name : "",
            coordX: this._coordX,
            coordY: this._coordY
        };
        // Push the log entry into the log array
        gameState.logs.push(logEntry);
    }

}