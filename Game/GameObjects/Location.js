/**
 * @class Location
 * @extends Phaser.GameObjects.Sprite
 * @description A location on the grid
 * @param {Phaser.Scene} scene - The scene that owns this sprite
 * @param {number} coordX - The x coordinate of the location
 * @param {number} coordY - The y coordinate of the location
 * @param {string} name - The name of the location
 * @param {string} fileName - The name of the file of the location
 */

class Location extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, name, fileName) { 

        super(scene, 0, 0, fileName);

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;

        this._coordX = coordX;
        this._coordY = coordY;  
        this._name = name;
        this._fileName = fileName;

        this.scene.add.existing(this);
        this.setOrigin(0.2, 0.2);
        this.setScale(0.6);
        this.setAlpha(0);

        gameState.locations.add(this);
    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }

    get name() {
        return this._name;
    }

    getfileName() {
        return this._fileName;
    }

    changeToWhite() {
        this.setTintFill(0xf7f7f7);
    }

    drawMe(delay) {
        this.scene.add.tween({
            targets: this,
            alpha: 1,
            duration: 600,
            ease: 'Cubic.easeIn',
            delay: delay,
        });
    }
}