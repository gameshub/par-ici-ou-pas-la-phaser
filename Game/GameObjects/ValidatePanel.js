class ValidatePanel extends Phaser.GameObjects.Container {
    constructor(scene, validateCallback) {
        super(scene, GAME_WIDTH / 2, GAME_HEIGHT / 2);
        scene.add.existing(this);

        this._scene = scene;
        this._validateCallback = validateCallback;
        this.setDepth(10);

        this.blackRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000, 0.7).setOrigin(0.5, 0.5);
        this.blackRectangle.setInteractive();

        this.modal = scene.add.image(0, 0, 'modal').setOrigin(0.5, 0.5);
        this.modalText = scene.add.text(0, - 10 , gameState.modalTextContent, { fontFamily: 'Roboto', fontSize: 24, color: '#000000', align: 'center', wordWrap: { width: 400, useAdvancedWrap: true } }).setOrigin(0.5, 0.5);
        
        this.buttonValidate = scene.add.sprite(this.modal.x + 100, this.modal.y + this.modal.height / 2, 'button_validate').setOrigin(0.5, 0.5).setDepth(gameState.layerModal).setScale(0.8);
        this.buttonValidate.on('pointerdown', () => {
            this._validateCallback();
            this.closePanel(false);
        });

        this.buttonReturn = scene.add.sprite(this.modal.x - 100, this.modal.y + this.modal.height / 2, 'button_return').setOrigin(0.5, 0.5).setDepth(gameState.layerModal).setScale(0.8);
        this.buttonReturn.on('pointerdown', () => {
            this.closePanel(true);
        });

        this.add(this.blackRectangle);
        this.add(this.modal);
        this.add(this.modalText);
        this.add(this.buttonValidate);
        this.add(this.buttonReturn);
 

        this.setScale(0);
        this.openPanel();    
    }

    /**
     * Open the panel with a scale animation
     */
    openPanel() {
        this._scene.tweens.add({
            targets: this,
            scale: 1,
            duration: 300,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this.buttonValidate.setInteractive();
                this.buttonReturn.setInteractive();
            }
        });
    }

    /**
     * Close the panel with a scale animation and create a validate button if needed
     * @param {boolean} mustCreateValidateButton 
     */
    closePanel(mustCreateValidateButton){
        this._scene.tweens.add({
            targets: this,
            scale: 0,
            duration: 300,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this.destroy();
                if(mustCreateValidateButton){
                    new ValidateButton(this._scene, GAME_WIDTH * 0.9, GAME_HEIGHT * 0.5);
                }
                
            }
        });
    }

    validateAnswer() {
        this.buttonValidate.disableInteractive();
        this.buttonReturn.disableInteractive();
        this.faddingRectangle = this._scene.add.rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000).setOrigin(0, 0).setDepth(10).setAlpha(0);
        this._scene.tweens.add({
            targets: this.faddingRectangle,
            alpha: 1,
            duration: 1000,
            ease: 'Power2',
            repeat: 0,
            delay: 0,
            onComplete: () => {
                this._scene.scene.stop(this._scene.scene.key);
                this._scene.scene.start('EndScene');
            }
        });
    }
}