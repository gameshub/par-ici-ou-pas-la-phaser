class Feather extends Phaser.GameObjects.Container {
    constructor(scene, x, y) {
        super(scene, x, y);
        this.scene.add.existing(this);     

        this._scene = scene;

        this.feather = scene.add.image(0, 0, 'feather').setOrigin(0, 1);

        this._x = x;
        this._y = y;

        this.add(this.feather);

        this.sineTween = this._scene.add.tween({
            targets: this.feather,
            x: this.feather.x + 40,
            duration: 100,
            ease: 'Sine.easeInOut',
            yoyo: true,
            repeat: -1
        });
        this.toggleSineTween(false);

    }

    drawLocation(location, delay) {
        const coordX = location.coordX;
        const coordY = location.coordY;
        //Move to the location
        this._scene.add.tween({
            targets: this,
            x: coordX * CELL_SIZE + GRID_OFFSET.x,
            y: coordY * CELL_SIZE + GRID_OFFSET.y,
            duration: 500,
            ease: 'Power2',
            delay: delay,
            //Start sine and move down
            onComplete: () => {
                this.toggleSineTween(true);
                this._scene.add.tween({
                    targets: this.feather,
                    y: this.feather.y + 60,
                    duration: 300,
                    ease: 'Linear',
                    //Move up and stop sine
                    onComplete: () => {
                        this._scene.add.tween({
                            targets: this.feather,
                            y: this.feather.y - 60,
                            duration: 200,
                            ease: 'Linear',
                            onComplete: () => {
                                this.toggleSineTween(false);
                            }
                        });
                    }
                });
            }
        });
    }

    moveBackToOriginalPosition(delay){
        this._scene.add.tween({
            targets: this,
            x: this._x,
            y: this._y,
            duration: 1000,
            ease: 'Power2',
            delay: delay
        });
    }

    toggleSineTween(doMove) {
        if (doMove) {
            this.sineTween.resume();
        } else {
            this.sineTween.pause();
        }
    }
}