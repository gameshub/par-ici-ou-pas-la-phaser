/**
 * @class Footprint
 * @extends Phaser.GameObjects.Sprite
 * @description Footprint spawned by the player
 * @param {Phaser.Scene} scene - The scene where the footprint is
 * @param {number} coordX - The x coordinate of the footprint
 * @param {number} coordY - The y coordinate of the footprint
 * @param {number} direction - The direction of the footprint
 */

class Footprint extends Phaser.GameObjects.Sprite {
    constructor(scene, coordX, coordY, direction) {
        super(scene, 0, 0, 'footprints');

        this.x = coordX * CELL_SIZE + GRID_OFFSET.x;
        this.y = coordY * CELL_SIZE + GRID_OFFSET.y;

        this._coordX = coordX;
        this._coordY = coordY;
        this._direction = direction;

        this.scene.add.existing(this);
        this.setOrigin(0.5, 0.5);
        this.setAlpha(0.7);
        this.setScale(0.6);

        switch (direction) {
            case 0:
                this.setOrigin(0, 0);
                this.setAngle(0);
                break;
            case 1:
                this.setOrigin(0, 1);
                this.setAngle(90);
                break;
            case 2:
                this.setOrigin(1, 1);
                this.setAngle(180);
                break;
            case 3:
                this.setOrigin(1, 0);
                this.setAngle(270);
                break;
        }

        gameState.footprints.add(this);

    }

    get coordX() {
        return this._coordX;
    }

    get coordY() {
        return this._coordY;
    }
}
