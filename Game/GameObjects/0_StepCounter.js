/**
 * @class StepCounter
 * @extends Phaser.GameObjects.Sprite
 * @param {Phaser.Scene} scene - The scene that owns this sprite.
 *  */

class StepCounter extends Phaser.GameObjects.Sprite {
    constructor(scene) {
        super(scene, GAME_WIDTH - (GAME_WIDTH - 12 * CELL_SIZE)/2, 20, 'step-counter');
        this.scene.add.existing(this);
        this.setOrigin(0.5, 0);
        this.setDepth(3);
        this._scene = scene;
        this._stepCount = 0;

        this.setAlpha(0); 
        this._stepCounterText = this.scene.add.text(GAME_WIDTH - (GAME_WIDTH - 12 * CELL_SIZE)/2, 100, this._stepCount, { fontFamily: 'Arial', fontSize: 32, color: '#ffffff', align: 'center' });
        this._stepCounterText.setOrigin(0.5, 0);
        this._stepCounterText.setAlpha(0);
        this._minSteps = this.calculateMinSteps();
    }
    get coordX() {
        return this._coordX;
    }
    get coordY() {
        return this._coordY;
    }
    get stepCount() {
        return this._stepCount;
    }

    get minSteps() {
        return this._minSteps;
    }


    /**
     * Increments the step counter
     */
    incrementStepCount() {
        this._stepCount++;
        this._stepCounterText.setText(this._stepCount);
    }

    /**
     * Calculates the minimum number of steps to reach all checkpoints
     */
    calculateMinSteps = () => {
        let minSteps = 0;
        minSteps += Math.abs(gameState.start.x - gameState.checkpointsData[0].x) + Math.abs(gameState.start.y - gameState.checkpointsData[0].y);
        for (let i = 0; i < gameState.checkpointsData.length-1; i++) {
            minSteps += Math.abs(gameState.checkpointsData[i].x - gameState.checkpointsData[i+1].x) + Math.abs(gameState.checkpointsData[i].y - gameState.checkpointsData[i+1].y);
        }
        return minSteps;
    }
}